#!/bin/bash

. ./tests.properties

print_edrs_count() {
	echo -e " $1 = `grep $2 $AUS_EVENT_LOG | wc -l | awk {'print $1'}` (expected=$3)"
}

cleanup() {
	$RHINO_CLIENT_CMD clearalarms sbb service=ServiceID[name=AUS,vendor=Polkomtel,version=$AUS_VERSION],sbb=SbbID[name=AUS,vendor=Polkomtel,version=$AUS_VERSION]
	$RHINO_CLIENT_CMD clearalarms sbb service=ServiceID[name=AUS,vendor=Polkomtel,version=$AUS_VERSION],sbb=SbbID[name=UanCharging,vendor=NSN,version=$UAN_VERSION]
	$RHINO_CLIENT_CMD rolloverlogfiles
	sleep 1
}

#####################################################################
#######################    TESTS EXECUTION    #######################
#####################################################################

echo -e "\n-------------------   Run all TCs   ---------------------"

cleanup
$OC_SIMULATOR_CMD

service_exceptions=`grep "\[AUS/AUS\].*Exception" $AUS_SERVICE_LOG | wc -l | awk {'print $1'}`
service_alarms_raised=`$RHINO_CLIENT_CMD listactivealarms | grep "plk.aus.LdapInconsistentData" | wc -l | awk {'print $1'}`


echo -e "\n-------------------------Summary-------------------------\n"
echo -e "Tests execution has finished with the following results:"
print_edrs_count "idp_received" "|7120|" "13"
print_edrs_count "con_sent" "|7109|" "4"
print_edrs_count "sci_sent" "|7107|" "2"
print_edrs_count "fci_sent" "|7108|" "2"
print_edrs_count "rrb_sent" "|7114|" "3"
print_edrs_count "ann_played" "|7112|" "1"
print_edrs_count "rc_sent" "|7122|" "1"
print_edrs_count "cue_sent" "|7123|" "7"
print_edrs_count "succ_ldap_queries" "|7121|" "10"
print_edrs_count "unsucc_ldap_queries" "|7124|" "3"
print_edrs_count "internal_errors" "|7127|" "3"
print_edrs_count "erb_abandon_received" "|7117|" "1"
print_edrs_count "prov_aborts_received" "|7125|" "0"
print_edrs_count "user_aborts_received" "|7126|" "0"
echo -e " service_alarms_raised = $service_alarms_raised (expected=3)"
echo -e " service_exceptions = $service_exceptions (expected=0)"
echo -e "\n"
