All E2E tests can be run with a single bash script:

	./run-tests.sh


Before any test is started, the following prerequisites ought to be fulfilled:

 - Rhino server is up and running

 - AUS solution is up and running:
	+ AUS service is successfully deployed
	+ LDAP schemas in CUPR are installed
	+ service subscription in NGIN DB is provisioned
	+ SIS composition for INAP DP3 is customized 

 - additional SIS network interface for simulated traffic has to be established, e.g.: 

	sis-console createnetworkinterface sis-in sis-in-sim-INAP IN local-sccp-address type=C7,ri=pcssn,pc=4,ssn=12 responder-sccp-address type=C7,ri=pcssn,pc=4,ssn=12 stack tcapsim tcapsim.listen-addresses localhost:10400 tcapsim.remote-addresses localhost:10300 tcapsim.gt-table ${/}opt${/}opencloud${/}rhinoB${/}rhino-connectivity${/}cgin${/}tcapsim-gt-table.txt
	sis-console enablenetworkinterface sis-in sis-in-sim-INAP
	sis-console reload sis-in
	sis-console deactivateraentity sis-in
	sis-console activateraentity sis-in

 - ldap test data defined in the db/tests/cupr.ldif file (attached to delivery package) have to be imported into CUPR:

	ldapadd -h localhost -D cn=admin,dc=cupr -w cuprPass -f cupr.ldif

 - sql test data defined in the attached db/tests/oracle.sql file (attached to delivery package) have to be imported into NGIN DB (remark: UNDESIRABLE_FAKE_IDS count result has to be equal to 0):		

	sqlplus ngin/ngin@ngin @/export/home/oracle/oracle.sql

- all environment specific variables included in tests.properties file are up-to-date

In order to perform databases cleanup upon successful test execution, please run:

	ldapdelete -h localhost -D cn=admin,dc=cupr -w cuprPass -f cupr_restore.ldif
	sqlplus ngin/ngin@ngin @/export/home/oracle/oracle_restore.sql