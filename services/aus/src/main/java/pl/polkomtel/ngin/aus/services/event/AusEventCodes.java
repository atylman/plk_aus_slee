package pl.polkomtel.ngin.aus.services.event;

import com.nsn.ploc.feniks.slee.nsnbaselib.log.EventCode;
import com.nsn.ploc.feniks.slee.nsnbaselib.log.EventFieldAnnotationAware;
import com.nsn.ploc.feniks.slee.nsnbaselib.log.annotations.EventFields;

import static pl.polkomtel.ngin.aus.services.event.AusEventHelper.*;

/**
 * <p>Enumeration used to assign unique event identifiers to service generated events.
 * 
 * <p>In addition to AUS specific EDRs, the following child SBBs:
 * <pre>UanCallConnect / UanUserInteraction / UanCharging </pre>
 * 
 * will publish their own events, which are as follows:
 * <p> 
 * <ul>
 * {@link com.nsn.gdo.feniks.slee.uan.common.UANEvents#EVENT_ANN_TIMEDOUT}
 * {@link com.nsn.gdo.feniks.slee.uan.common.UANEvents#EVENT_CHRGING_MODIFIED}
 * {@link com.nsn.gdo.feniks.slee.uan.common.UANEvents#EVENT_BILLING_ADDR_MODIFIED}
 * {@link com.nsn.gdo.feniks.slee.uan.common.UANEvents#EVENT_CONNECT_SENT}
 * {@link com.nsn.gdo.feniks.slee.uan.common.UANEvents#EVENT_RRBCSM_SENT}
 * {@link com.nsn.gdo.feniks.slee.uan.common.UANEvents#EVENT_ANN_PLAYED}
 * {@link com.nsn.gdo.feniks.slee.uan.common.UANEvents#EVENT_CONNECT_FAILED}
 * {@link com.nsn.gdo.feniks.slee.uan.common.UANEvents#EVENT_O_ABANDON_RECEIVED}
 * </ul>
 * 
 * @author adam.tylman
 * @see com.nsn.gdo.feniks.slee.uan.common.UANEvents
 */
public enum AusEventCodes implements EventCode, EventFieldAnnotationAware {
		
	@EventFields(defaultFields = true, fields = {CDPA_NUM_TON, CGPA_NUM_TON, LOC_INFO, REDPA_NUM, REDPA_NUM_TON})
	EVENT_IDP_RECEIVED(20),
	
	@EventFields(defaultFields = true, fields = {PREFIX, ANN_ID, FCI_ID, SCI_ID, ADDITIONAL_INFO})
	EVENT_SERVICE_CHK_OK(21),
    
	@EventFields(defaultFields = true)
	EVENT_RC_SENT(22),
	
	@EventFields(defaultFields = true, fields = {CAUSE, ADDITIONAL_INFO})
	EVENT_CUE_SENT(23),
    
	@EventFields(defaultFields = true, fields = {CAUSE, ADDITIONAL_INFO})
	EVENT_SERVICE_CHK_FAILED(24),

	@EventFields(defaultFields = true, fields = {CAUSE, ADDITIONAL_INFO})
	EVENT_PROVIDER_ABORT_RECEIVED(25),
	
	@EventFields(defaultFields = true, fields = {CAUSE, ADDITIONAL_INFO})
	EVENT_USER_ABORT_RECEIVED(26),
	
	@EventFields(defaultFields = true, fields = {CAUSE, ADDITIONAL_INFO})
	EVENT_INTERNAL_ERROR(27);

	public static final int SUBSYSTEM_IDENTIFIER = 71;
	
    private final int eventCode;

    private AusEventCodes(final int eventCode) {
        this.eventCode = SUBSYSTEM_IDENTIFIER * 100 + eventCode;
    }

    /**
     * Return an event code value
     * @see com.nsn.ploc.feniks.slee.nsnbaselib.log.EventCode#getEventCode()
     */
    @Override
    public int getEventCode() {
        return eventCode;
    }

    /**
     * Returns the name of the current enum constant 
     * @see com.nsn.ploc.feniks.slee.nsnbaselib.log.EventFieldAnnotationAware#getName()
     */
	@Override
	public String getName() {
		return this.name();
	}
}
