package pl.polkomtel.ngin.aus.services.event;

import org.apache.commons.lang.StringUtils;

import pl.polkomtel.ngin.aus.services.common.AusContext;

import com.nsn.ploc.feniks.slee.nsnbaselib.log.EventHelper;
import com.nsn.ploc.feniks.slee.nsnbaselib.log.annotations.EventMethod;
import com.nsn.ploc.feniks.slee.nsnbaselib.utils.StringConverterUtil;

/**
 * A factory class to be used for events creation.
 *
 * @author adam.tylman
 *
 */
public class AusEventHelper extends EventHelper {

	public static final String CDPA_NUM = "CDPA_NUM";
	public static final String CDPA_NUM_TON = "CDPA_NUM_TON";
	public static final String CGPA_NUM = "CGPA_NUM";
	public static final String CGPA_NUM_TON = "CGPA_NUM_TON";
	public static final String REDPA_NUM = "REDPA_NUM";
	public static final String REDPA_NUM_TON = "REDPA_NUM_TON";
	public static final String LOC_INFO = "LOC_INFO";
	public static final String CALL_SCEN = "CALL_SCEN";
	public static final String PREFIX = "PREFIX";
	public static final String ANN_ID = "ANN_ID";
	public static final String FCI_ID = "FCI_ID";
	public static final String SCI_ID = "SCI_ID";
	public static final String CAUSE = "CAUSE";
	public static final String ADDITIONAL_INFO = "ADDITIONAL_INFO";
	
	private final AusContext context;
	private final String errorCause;
	private final String additionalInfo;
	
	/**
	 * Default constructor
	 * @param context a reference to AUS context that holds all necessary data
	 * @param errorCause a value of {@link #CAUSE} field in EDR to be published
	 * @param additionalInfo a value of {@link #ADDITIONAL_INFO} field in EDR to be published
     */
	public AusEventHelper(final AusContext context, final String errorCause, final String additionalInfo) {
		this.context = context;
		this.errorCause = StringUtils.isNotBlank(errorCause) ? errorCause : "";
		this.additionalInfo = StringUtils.isNotBlank(additionalInfo) ? additionalInfo : "";
	}
	
	/**
	 * Holds default event field descriptions to avoid declaring many times the same events
     * @see com.nsn.ploc.feniks.slee.nsnbaselib.log.EventHelper#getDefaultEventFields()
     */
	@Override
	protected String[] getDefaultEventFields() {
		return new String[] { CDPA_NUM, CGPA_NUM, CALL_SCEN };
	}
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = CALL_SCEN)
    public String getCallScenario() {
        return Integer.toString(context.getScenario().getNumeral());
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = PREFIX)
    public String getRoutingPrefix() {
        return context.getPrefixNumber();
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = ANN_ID)
    public String getAnnId() {
        return context.getAnnouncementId()!=null ? context.getAnnouncementId().toString() : "";
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = FCI_ID)
    public String getFciId() {
        return context.getChargingFciId()!=null ? context.getChargingFciId().toString() : "";
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = SCI_ID)
    public String getSciId() {
        return context.getChargingSciId()!=null ? context.getChargingSciId().toString() : "";
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = CDPA_NUM)
    public String getCdPartyNumber() {
        return context.getCalledNumber();
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = CDPA_NUM_TON)
    public String getCdPartyNumberTon() {
        return Integer.toString(context.getCalledNumberTON());
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = CGPA_NUM)
    public String getCgPartyNumber() {
        return context.getCallingNumberFormatted();
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = CGPA_NUM_TON)
    public String getCgPartyNumberTon() {
        return Integer.toString(context.getCallingNumberTONFormatted());
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = LOC_INFO)
    public String getLocationInfo() {
        return StringConverterUtil.getHexString(context.getLocationInfo());
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = REDPA_NUM)
    public String getRedirectingPartyNumber() {
        return context.getRedirectingNumberFormatted();
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = REDPA_NUM_TON)
    public String getRedirectionInfo() {
        return Integer.toString(context.getRedirectingNumberTONFormatted());
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = CAUSE)
    public String getCause() {
        return this.errorCause;
    }
	
	/**
     * Returns value for the specified key in event record log
     * @return String value to be inserted into event record
     */
	@EventMethod(event = ADDITIONAL_INFO)
    public String getAdditionalInfo() {
        return this.additionalInfo;
    }
}
