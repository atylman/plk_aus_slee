package pl.polkomtel.ngin.aus.services.common;

import javax.slee.ActivityContextInterface;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.AlarmLevel;
import javax.slee.facilities.TraceLevel;

import netscape.ldap.LDAPException;

import org.apache.commons.lang.StringUtils;

import pl.polkomtel.ngin.aus.services.AusSbb;
import pl.polkomtel.ngin.aus.services.alarm.AusAlarms;
import pl.polkomtel.ngin.aus.services.event.AusEventCodes;
import pl.polkomtel.ngin.aus.services.event.EventReporterCallback;

import com.nsn.ploc.feniks.slee.nsnbaselib.log.TracerCallback;
import com.opencloud.slee.resources.ldap.LDAPActivityContextInterfaceFactory;
import com.opencloud.slee.resources.ldap.LDAPProvider;
import com.opencloud.slee.resources.ldap.LDAPSearchActivity;
import com.opencloud.slee.resources.ldap.LDAPSearchResultEvent;
import com.opencloud.slee.resources.ldap.objects.Attribute;
import com.opencloud.slee.resources.ldap.objects.ConnectionException;
import com.opencloud.slee.resources.ldap.objects.Entry;
import com.opencloud.slee.resources.ldap.objects.SearchException;
import com.opencloud.slee.resources.ldap.objects.SearchResults;


/**
 * Helper class used for all LDAP operations performed on CUPR.
 *
 * @author adam.tylman
 */
public class LdapUtils {

	private static final int DEFAULT_SEARCH_SCOPE = LDAPSearchActivity.SCOPE_BASE;
	
	private static final String ATTRIBUTE_MSISDN = "msisdn";
	private static final String ATTRIBUTE_ISBLOCKED = "isBlocked";
	private static final String ATTRIBUTE_PREFIXNUMBER = "prefixNumber";
	private static final String ATTRIBUTE_ANNOUNCEMENTID = "announcementId";
	private static final String ATTRIBUTE_CHARGINGSCIID = "chargingSciId";
	private static final String ATTRIBUTE_CHARGINGFCIID = "chargingFciId";
	private static final String ATTRIBUTE_CHARGINGFCISERVIDENT = "chargingFciServIdent";

	private static final String PROFILE_SEARCH_BASE_DISTINGUISHED_NAME = "msisdn=%s,ou=profiles,ou=aus,ou=ngin,ou=services,o=polkomtel,dc=cupr";
	private static final String PROFILE_SEARCH_FILTER = "(objectClass=ausProfile)";
	private static final String[] PROFILE_SEARCH_ATTRIBUTES = new String[] { ATTRIBUTE_MSISDN, ATTRIBUTE_ISBLOCKED, ATTRIBUTE_PREFIXNUMBER, ATTRIBUTE_ANNOUNCEMENTID, ATTRIBUTE_CHARGINGSCIID, ATTRIBUTE_CHARGINGFCIID, ATTRIBUTE_CHARGINGFCISERVIDENT };

	/**
	 * Sends LDAP query to CUPR.
	 * 
	 * @param msisdnToSearch MSISDN number 
	 * @param ldapProvider LDAP provider used for search operation
	 * @param ldapAciFactory LDAP aci factory used for search operation
	 * @param callback callback interface to SBB needed for tracing
	 * @return ACI object that SBB entity should attach to
	 * @throws ConnectionException if any connection error to CUPR occurs
	 * @throws SearchException if sth with search operation went wrong
	 */
	public static ActivityContextInterface sendLdapQuery(final String msisdnToSearch, final LDAPProvider ldapProvider, final LDAPActivityContextInterfaceFactory ldapAciFactory, final TracerCallback callback) throws ConnectionException, SearchException {
		final String bdn = String.format(PROFILE_SEARCH_BASE_DISTINGUISHED_NAME, msisdnToSearch);
		if (callback.isTraceable(TraceLevel.FINEST))
            callback.tracef(TraceLevel.FINEST, "sendLdapQuery method: the following base DN is going to be serched for [%s]", bdn);
		
		try {
			final LDAPSearchActivity ldapActivity = ldapProvider.getNewSearchActivity();
			final ActivityContextInterface ldapAci = ldapAciFactory.getActivityContextInterface(ldapActivity);
			ldapActivity.search(bdn, DEFAULT_SEARCH_SCOPE, PROFILE_SEARCH_FILTER, PROFILE_SEARCH_ATTRIBUTES, false, null);
			return ldapAci;
		} catch (ConnectionException | SearchException ex) {
			callback.tracef(TraceLevel.WARNING, ex, "sendLdapQuery method: unexpected exception was thrown for base DN [%s]", bdn);
			throw ex;
		}
	}
	
	/**
	 * Method for processing LDAP query successful results and filling up AUS profile with data found in CUPR.
	 * 
	 * @param event search result to process
	 * @param ctx AUS profile to fill up
	 * @param tcb callback interface to SBB needed for tracing
	 * @return processing result
	 */
	public static boolean processLdapSearchResult(final LDAPSearchResultEvent event, final AusContext ctx, final TracerCallback tcb, final AlarmFacility af) {
		if (tcb.isTraceable(TraceLevel.FINEST))
			tcb.tracef(TraceLevel.FINEST, "processLdapSearchResult method: the following search result was received [%s]", event);

		final SearchResults searchResults = event.getResult();
		if (searchResults == null || !searchResults.hasMoreElements()) {
			tcb.tracef(TraceLevel.FINE, "processLdapSearchResult method: search result is null or empty!!");
			return false;
		}

		try {
			final Entry ldapEntry = searchResults.next();
			if (ldapEntry != null) {
				ctx.setIsBlocked(getBooleanValue(ldapEntry, ATTRIBUTE_ISBLOCKED, tcb));
				ctx.setPrefixNumber(getStringValue(ldapEntry, ATTRIBUTE_PREFIXNUMBER, tcb));
				ctx.setAnnouncementId(getIntValue(ldapEntry, ATTRIBUTE_ANNOUNCEMENTID, tcb));
				ctx.setChargingSciId(getIntValue(ldapEntry, ATTRIBUTE_CHARGINGSCIID, tcb));
				ctx.setChargingFciId(getIntValue(ldapEntry, ATTRIBUTE_CHARGINGFCIID, tcb));
				ctx.setServiceIdentifier(getStringValue(ldapEntry, ATTRIBUTE_CHARGINGFCISERVIDENT, tcb));
				
				if(ctx.validateLdapParamsConsistency()) {
					if(tcb.isTraceable(TraceLevel.FINEST))
						tcb.tracef(TraceLevel.FINEST, "processLdapSearchResult method: AUS context after filling in [%s]", ctx.toString());
					return true;
				} else {
					tcb.tracef(TraceLevel.WARNING, "processLdapSearchResult method: AUS context is incomplete [%s]", ctx.toString());
					af.raiseAlarm(
							String.format("%s.%s", AusSbb.TRACER_NAME, AusAlarms.LDAP_INCONSISTENT_DATA.getAlarmType()),
							String.format(PROFILE_SEARCH_BASE_DISTINGUISHED_NAME, getStringValue(ldapEntry, ATTRIBUTE_MSISDN, tcb)),
							AusAlarms.LDAP_INCONSISTENT_DATA.getAlarmLevel(),
							AusAlarms.LDAP_INCONSISTENT_DATA.getFinalMessage());
				}
			} else
				tcb.tracef(TraceLevel.FINE, "processLdapSearchResult method: search result entry is null!!");
		} catch (final SearchException ex) {
			tcb.tracef(TraceLevel.WARNING, ex, "processLdapSearchResult method: unexpected exception was thrown!!");
		}
		
		return false;
	}
	
	/**
	 * Method for processing LDAP query error results.
	 * 
	 * @param resultEvent error result to process
	 * @param ctx AUS profile to fill up
	 * @param ercb callback interface to SBB needed for EDRs generation
	 * @param tcb callback interface to SBB needed for tracing
	 * @param af used when any alarm should be raised
	 */
	public static void processLdapErrorResult(final LDAPSearchResultEvent resultEvent, final AusContext ctx, final EventReporterCallback ercb, final TracerCallback tcb, final AlarmFacility af) {
		if (tcb.isTraceable(TraceLevel.FINEST))
			tcb.tracef(TraceLevel.FINEST, "processLdapErrorResult method: the following error result was received [%s]", resultEvent);

		final String errMsg = String.format("for msisdn [%s], result code [%d], message [%s]", ctx.getMsisdn(), resultEvent.getErrorCode(), resultEvent.getErrorMessage());
		switch (resultEvent.getErrorCode()) {
			case LDAPException.NO_SUCH_OBJECT:
			case LDAPException.NO_RESULTS_RETURNED:
				tcb.tracef(TraceLevel.FINE, "processLdapErrorResult method: LDAP entry not found %s", errMsg);
				ercb.publishReportEvent(
						AusEventCodes.EVENT_SERVICE_CHK_FAILED,
						Integer.toString(resultEvent.getErrorCode()) + ":" + LDAPException.errorCodeToString(resultEvent.getErrorCode()),
						"No result: " + resultEvent.getErrorMessage());
				break;
			case LDAPException.TIME_LIMIT_EXCEEDED:
			case LDAPException.LDAP_TIMEOUT:
				tcb.tracef(TraceLevel.WARNING, "processLdapErrorResult method: LDAP timeout %s", errMsg);
				ercb.publishReportEvent(
						AusEventCodes.EVENT_SERVICE_CHK_FAILED,
						Integer.toString(resultEvent.getErrorCode()) + ":" + LDAPException.errorCodeToString(resultEvent.getErrorCode()),
						"Timeout: " + resultEvent.getErrorMessage());
				af.raiseAlarm(String.format("%s.LdapConnectionError", AusSbb.TRACER_NAME), String.format("resource-adaptor-object-name:%s", AusSbb.LDAP_PROVIDER_JNDI_NAME), AlarmLevel.MAJOR, "Timeout occured upon sending LDAP search query");
				break;
			default:
				tcb.tracef(TraceLevel.WARNING, "processLdapErrorResult method: other LDAP error %s", errMsg);
				ercb.publishReportEvent(
						AusEventCodes.EVENT_SERVICE_CHK_FAILED,
						Integer.toString(resultEvent.getErrorCode()) + ":" + LDAPException.errorCodeToString(resultEvent.getErrorCode()),
						"General error: " + resultEvent.getErrorMessage());
				break;
		}
	}

	private static String getStringValue(final Entry ldapEntry, final String attributeName, final TracerCallback callback) {
		try {
			final Attribute attribute = ldapEntry.getAttribute(attributeName);
			return attribute != null ? attribute.getStringValue() : null;
		} catch (final Exception ex) {
			callback.tracef(TraceLevel.WARNING, ex, "getStringValue method: exception thrown while getting [%s] attribute from the following LDAP entry [%s]", attributeName, ldapEntry);
			return null;
		}
	}

	private static Integer getIntValue(final Entry ldapEntry, final String attributeName, final TracerCallback callback) {
		try {
			final Attribute attribute = ldapEntry.getAttribute(attributeName);
			return attribute != null && StringUtils.isNotBlank(attribute.getStringValue()) ? Integer.valueOf(attribute.getStringValue()) : null;
		} catch (final Exception ex) {
			callback.tracef(TraceLevel.WARNING, ex, "getStringValue method: exception thrown while getting [%s] attribute from the following LDAP entry [%s]", attributeName, ldapEntry);
			return null;
		}
	}

	private static Boolean getBooleanValue(final Entry ldapEntry, final String attributeName, final TracerCallback callback) {
		try {
			final Attribute attribute = ldapEntry.getAttribute(attributeName);
			return attribute != null && StringUtils.isNotBlank(attribute.getStringValue()) ? Boolean.valueOf(attribute.getStringValue()) : null;
		} catch (final Exception ex) {
			callback.tracef(TraceLevel.WARNING, ex, "getStringValue method: exception thrown while getting [%s] attribute from the following LDAP entry [%s]", attributeName, ldapEntry);
			return null;
		}
	}
}
