package pl.polkomtel.ngin.aus.services.common;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Provides tools needed to perform serialization/deserialization of local variables while trying to serialize/deserialize 
 * state of the entire object that extends this class.
 *
 * @author adam.tylman
 */
public abstract class FastSerializableEntity {

	protected String readString(final DataInput in) throws IOException {
		return in.readBoolean() ? in.readUTF() : null;
	}
	
	protected void writeString(final DataOutput out, final String arg) throws IOException {
		if (arg == null) {
			out.writeBoolean(false);
		} else {
			out.writeBoolean(true);
			out.writeUTF(arg);
		}
	}
	
	protected Integer readInteger(final DataInput in) throws IOException {
		return in.readBoolean() ? Integer.valueOf(in.readInt()) : null;
	}
	
	protected void writeInteger(final DataOutput out, final Integer arg) throws IOException {
		if (arg == null) {
			out.writeBoolean(false);
		} else {
			out.writeBoolean(true);
			out.writeInt(arg.intValue());;
		}
	}
	
	protected Boolean readBoolean(final DataInput in) throws IOException {
		return in.readBoolean() ? Boolean.valueOf(in.readBoolean()) : null;
	}
	
	protected void writeBoolean(final DataOutput out, final Boolean arg) throws IOException {
		if (arg == null) {
			out.writeBoolean(false);
		} else {
			out.writeBoolean(true);
			out.writeBoolean(arg.booleanValue());
		}
	}
}
