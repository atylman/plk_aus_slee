package pl.polkomtel.ngin.aus.services;

import java.util.Arrays;

import javax.slee.ActivityContextInterface;
import javax.slee.InitialEventSelector;
import javax.slee.InvalidArgumentException;
import javax.slee.facilities.TimerEvent;
import javax.slee.facilities.TraceLevel;

import pl.polkomtel.ngin.aus.services.common.LdapUtils;
import pl.polkomtel.ngin.aus.services.event.AusEventCodes;
import pl.polkomtel.ngin.aus.services.event.AusEventHelper;

import com.nsn.gdo.feniks.slee.uan.common.EventMap;
import com.nsn.gdo.feniks.slee.uan.common.UANEvents;
import com.nsn.gdo.feniks.slee.uan.common.UIGlobals;
import com.nsn.gdo.feniks.slee.uan.common.localinterfaceargs.UIOutParam;
import com.nsn.ploc.feniks.slee.nsnbaselib.utils.StringConverterUtil;
import com.opencloud.slee.resources.cgin.DialogOpenRequestEvent;
import com.opencloud.slee.resources.cgin.DialogProviderAbortEvent;
import com.opencloud.slee.resources.cgin.DialogUserAbortEvent;
import com.opencloud.slee.resources.cgin.callcontrol.CCEventReportBCSMArg;
import com.opencloud.slee.resources.cgin.callcontrol.CCEventTypeBCSM;
import com.opencloud.slee.resources.cgin.callcontrol.events.CCEventReportBCSMRequestEvent;
import com.opencloud.slee.resources.cgin.callcontrol.events.CCInitialDPRequestEvent;
import com.opencloud.slee.resources.cgin.nokia_inap_cs1.metadata.NCS1ApplicationContexts;
import com.opencloud.slee.resources.ldap.LDAPSearchResultEvent;
import com.opencloud.slee.resources.sis.script.activity.CompositionActivityExtensionsProvider;

/**
 * Defines methods for processing incoming events.
 * 
 * @author adam.tylman
 */
public abstract class AusEventHandler extends AusBaseSbb {

	/**
	 * Default SLEE InitialEventSelector method used for filtering out inbound dialogs and IDPs of our interest.
	 * 
	 * @param ies SLEE {@link InitialEventSelector}
	 * @return {@link InitialEventSelector} object after customization
	 */
	public InitialEventSelector initialEventSelect(final InitialEventSelector ies) {
        final DialogOpenRequestEvent openRequest = (DialogOpenRequestEvent) ies.getEvent();
        if(isTraceable(TraceLevel.FINEST))
        	getSbbTracer().trace(TraceLevel.FINEST, String.format("IES method: IES.address [%s], DialogOpenRequestEvent [%s]", ies.getAddress(), openRequest));
        
        boolean eventSupported = false;
        if(openRequest.getApplicationContext() == NCS1ApplicationContexts.core_INAP_CS1_SSP_to_SCP_AC)
        	eventSupported = true;
        
        if(isTraceable(TraceLevel.FINE))
        	getSbbTracer().trace(TraceLevel.FINE, "Initial event selection validation result: " + eventSupported);
        ies.setInitialEvent(eventSupported);
        return ies;
    }

	/**
	 * @slee.event-method
	 *	 initial-event="True"
	 *   event-type-name="com.opencloud.slee.resources.cgin.DialogOpenRequest"
	 *   event-type-vendor="OpenCloud"
	 *   event-type-version="${cgin.slee.version}"
	 *   initial-event-select-variable="ActivityContext"
	 *   initial-event-selector-method-name="initialEventSelect"
	 */
	public void onOpenRequest(final DialogOpenRequestEvent event, final ActivityContextInterface aci) {
		CCInitialDPRequestEvent idp = null;
		boolean dialogAccepted = false;
		try {
			event.getDialog().acceptDialog();
			dialogAccepted = true;
			
			if(event.getComponentEvents().length >= 0 && event.getComponentEvents()[0] instanceof CCInitialDPRequestEvent) {
				idp = (CCInitialDPRequestEvent) event.getComponentEvents()[0];
				if(idp.getDialog().getProvider() instanceof CompositionActivityExtensionsProvider) {
					if(prepareTraceContext(idp) && prepareInapContext(idp)) {
						dumpAusContext("onOpenRequest");
						publishReportEvent(AusEventCodes.EVENT_IDP_RECEIVED);
						getEndpoints().ssp.setAci(aci);
						raiseInputAndExecuteFsm(getInputs().local.context_creation_success, event);
					}
					else
						throw new InvalidArgumentException("onOpenRequest method: Call context creation failed due to missing params!!");
				} else
					throw new InvalidArgumentException("onOpenRequest method: Composition activity of an incoming DialogOpenRequest cannot be retrieved!!");
			} else
				throw new InvalidArgumentException("onOpenRequest method: Component portion of an incoming DialogOpenRequest is invalid!!");
		
		} catch (final Exception ex) {
			if (getSessionIdImpl() == null)
				generateSessionId();
			
			final StringBuilder errLog = new StringBuilder("onOpenRequest method: exception occurred for:\n\n");
			errLog.append("DialogOpenRequestEvent:\n").append(event).append("\n\n");
			errLog.append("InitialDPRequestEvent:\n").append(idp).append("\n\n");
			errLog.append("AusContext:\n").append(getAusContextCache()).append("\n\n");
			
			severe(errLog.toString(), ex);
			publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, null, ex.getMessage());
			
			if(dialogAccepted) {
				getEndpoints().ssp.setAci(aci);
				raiseInputAndExecuteFsm(getInputs().local.context_creation_failed, event);
			}
			else
				detachAllActivities();
		}
	}
	
	/**
	 * @slee.event-method
	 *	 initial-event="False"
	 *   event-type-name="com.opencloud.slee.resources.ldap.LDAPSearchResultEvent"
	 *   event-type-vendor="OpenCloud"
	 *   event-type-version="${ldap.slee.version}"
	 */
    public void onLdapSearchResultEvent(final LDAPSearchResultEvent event, final ActivityContextInterface aci) {
        if(!event.hasErrorInfo() && LdapUtils.processLdapSearchResult(event, getAusContextCache(), this, getAlarmFacility()))
        	raiseInputAndExecuteFsm(getInputs().cupr.service_details_found, event);
        else {
        	LdapUtils.processLdapErrorResult(event, getAusContextCache(), this, this, getAlarmFacility());
        	raiseInputAndExecuteFsm(getInputs().cupr.ldap_error, event);
        }
    }
    
    /**
     * @slee.event-method
     *	 initial-event="False"
     *	 event-type-name="com.opencloud.slee.resources.cgin.callcontrol.eventReportBCSMRequest"
     *	 event-type-vendor="OpenCloud"
     *	 event-type-version="${cgin.slee.version}"
 	 */
    public void onEventReportBcsmRequest(final CCEventReportBCSMRequestEvent event, final ActivityContextInterface aci) {
    	final CCEventReportBCSMArg erbArg = event.getArgument();
		warn(String.format("onEventReportBcsmRequest method: ERB with event type [%s] received, most likely during announcement playing due to subs abort, dialog's state is [%s]",
							CCEventTypeBCSM.fromValue(erbArg.getEventTypeBCSM().intValue()).toString(),
							event.getDialog().getDialogState().name()));

		//EDR is not generated by UAN UI child SBB so consequently, we have to take over that duty  
        if(erbArg.getEventTypeBCSM().equals(CCEventTypeBCSM.oAbandon)) {
        	setEventLogId(AusSbb.TRACER_NAME);
        	setSubSystemIdentifier(Integer.valueOf(AusEventCodes.SUBSYSTEM_IDENTIFIER * 100));
        	final EventMap eventMap = new EventMap(AusSbb.SERVICE_NAME, UANEvents.EVENT_O_ABANDON_RECEIVED);
        	eventMap.put(AusEventHelper.ADDITIONAL_INFO, "State:"+getCurrentState().getStateIdentifier().name());
        	publishEvent(eventMap);
        }
    }
    
    /**
     * @slee.event-method
     *	 initial-event="False"
     *	 event-type-name="com.opencloud.slee.resources.cgin.DialogProviderAbort"
     *	 event-type-vendor="OpenCloud"
     *	 event-type-version="${cgin.slee.version}"
 	 */
    public void onProviderAbort(final DialogProviderAbortEvent event, final ActivityContextInterface aci) {
		warn(String.format("onProviderAbort method: transaction ceased by SSP with the following reason [%s], dialog's state is [%s]",
						event.getAbortReason().name(), event.getDialog().getDialogState().name()));
		publishReportEvent(
				AusEventCodes.EVENT_PROVIDER_ABORT_RECEIVED,
				"TCAP OTID:" + StringConverterUtil.getHexString(event.getDialog().getRemoteTransactionID()),
				event.toString());
    	raiseInputAndExecuteFsm(getInputs().ssp.provider_abort_received, event);
    }
     
    /**
  	 * @slee.event-method
  	 *	 initial-event="False"
  	 *   event-type-name="com.opencloud.slee.resources.cgin.DialogUserAbort"
  	 *   event-type-vendor="OpenCloud"
  	 *   event-type-version="${cgin.slee.version}"
  	 */
    public void onUserAbort(final DialogUserAbortEvent event, final ActivityContextInterface aci) {
		warn(String.format("onUserAbort method: transaction ceased by SSP with the following user info %s, dialog's state is [%s]",
						Arrays.toString(event.getUserInformation()), event.getDialog().getDialogState().name()));
		publishReportEvent(
				AusEventCodes.EVENT_USER_ABORT_RECEIVED,
				"TCAP OTID:" + StringConverterUtil.getHexString(event.getDialog().getRemoteTransactionID()),
				event.toString());
		raiseInputAndExecuteFsm(getInputs().ssp.user_abort_received, event);
    }
    
    /**
  	 * @slee.event-method
  	 *	 initial-event="False"
  	 *   event-type-name="javax.slee.facilities.TimerEvent"
  	 *   event-type-vendor="javax.slee"
  	 *   event-type-version="1.0"
  	 */
    public void onTimerExpiry(final TimerEvent event, final ActivityContextInterface aci) {
		if(event.getTimerID().equals(getAnnTimerId())) {
			warn(String.format("onTimerExpiry method: play ann timer expired [%s] due to lack of response from UI SBB, dialog's state is [%s]",
					event.getTimerID().toString(),
					getDialogStateFromAci(aci)));
			raiseInputAndExecuteFsm(getInputs().ssp.ann_timeout, event);
		} else
			info(String.format("onTimerExpiry method: timeout ID [%s] does not match (UI module is the owner) and thus, no action will be taken", event.getTimerID()));
    }
    
    /**
     * Implementation of callback method from {@link com.nsn.gdo.feniks.slee.uan.common.callback.UserInteractionCallBack} that is invoked upon announcement completion.
     * @param uiOutParam output returned by user interaction child SBB
	 * @see com.nsn.gdo.feniks.slee.uan.common.callback.UANCallBack#announcementMessageCB(com.nsn.gdo.feniks.slee.uan.common.localinterfaceargs.UIOutParam)
	 */
    @Override
    public void announcementMessageCB(final UIOutParam uiOutParam) {
    	if(uiOutParam.getResultCode() == UIGlobals.UI_SUCCESS)
    		raiseInputAndExecuteFsm(getInputs().ssp.ann_played, uiOutParam);
    	else
    		raiseInputAndExecuteFsm(getInputs().ssp.ann_failed, uiOutParam);
    }
}
