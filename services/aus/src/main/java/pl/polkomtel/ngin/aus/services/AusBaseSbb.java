package pl.polkomtel.ngin.aus.services;

import static javax.slee.facilities.TraceLevel.FINEST;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.CreateException;
import javax.slee.facilities.TimerID;
import javax.slee.facilities.TraceLevel;

import pl.polkomtel.ngin.aus.services.alarm.AusAlarms;
import pl.polkomtel.ngin.aus.services.common.AusContext;
import pl.polkomtel.ngin.aus.services.event.AusEventCodes;
import pl.polkomtel.ngin.aus.services.event.EventReporterCallback;
import pl.polkomtel.ngin.aus.services.stats.AusUsageParameters;

import com.nsn.gdo.feniks.slee.uan.callconnect.CallConnectSbbLocalInterface;
import com.nsn.gdo.feniks.slee.uan.common.UANChargingGlobals;
import com.nsn.gdo.feniks.slee.uan.common.callback.UANCallBackInterface;
import com.nsn.gdo.feniks.slee.uan.common.localinterfaceargs.CallConnectInParam;
import com.nsn.gdo.feniks.slee.uan.common.localinterfaceargs.ChargingInParam;
import com.nsn.gdo.feniks.slee.uan.common.localinterfaceargs.UIInParam;
import com.nsn.gdo.feniks.slee.uan.common.localinterfaceargs.UanInParam;
import com.nsn.gdo.feniks.slee.uan.userinteraction.IVRInterfaceSbbLocalInterface;
import com.nsn.ploc.feniks.slee.nsnbaselib.InapCallContext;
import com.nsn.ploc.feniks.slee.nsnbaselib.consts.TeleserviceCode;
import com.nsn.ploc.feniks.slee.nsnbaselib.log.TracerCallback;
import com.nsn.ploc.feniks.slee.resources.eventreporter.SessionTraceContext;
import com.nsn.ploc.feniks.slee.sis.script.CommonVariableHelper;
import com.opencloud.sce.fsmtool.FSMInput;
import com.opencloud.slee.resources.cgin.callcontrol.CCDialog;
import com.opencloud.slee.resources.cgin.callcontrol.events.CCInitialDPRequestEvent;
import com.opencloud.slee.resources.cgin.etsi_inap_cs1.CS1InitialDPArg;
import com.opencloud.slee.resources.in.datatypes.cc.CalledPartyNumber;
import com.opencloud.slee.resources.in.datatypes.cc.CalledPartyNumber.NumberingPlan;

/**
 * A superclass that implements low level atomic procedures which will be used further on by other subclasses in order to
 * execute more complex service logic. 
 *
 * @author adam.tylman
 */
public abstract class AusBaseSbb extends AusSbbStateMachine implements TracerCallback, EventReporterCallback {

	protected AusContext ausContextCache = null;
	
	protected AusContext getAusContextCache() {
		return ausContextCache == null ? ausContextCache = getAusContext() : ausContextCache;
	}
	
	protected void dumpAusContext(final String methodName) {
		if(isTraceable(TraceLevel.FINEST))
			finest(String.format("%s method: AUS invocation context printout: [%s]", methodName, getAusContextCache()));
	}
	
	protected void raiseInputAndExecuteFsm(final FSMInput input, final Object event) {
        getInputScheduler().raise(input, event);
        execute();
    }
	
	protected boolean prepareTraceContext(final CCInitialDPRequestEvent idp) {
		final SessionTraceContext sessionId = CommonVariableHelper.loadSessionTraceContext(idp.getDialog(), AusBaseSbb.this);
		if(sessionId != null) {
			sessionId.setTracerName(AusSbb.TRACER_NAME);
			setSessionIdImpl(sessionId);
			if(sessionId.isSubscriberTraceable())
				setSubscriberTraceLevel(TraceLevel.FINEST);
			return true;
		} else {
			generateSessionId();
			setSubscriberTraceLevel(TraceLevel.OFF);
			return false;
		}
	}

	protected boolean prepareInapContext(final CCInitialDPRequestEvent idp) {
		ausContextCache = new AusContext();
		final CS1InitialDPArg idpArg = (CS1InitialDPArg) idp.getArgument();
		if(idpArg.hasLocationNumber() && idpArg.getLocationNumber().getEncodedForm().length > 8)
			ausContextCache.setLocationInfo(idpArg.getLocationNumber().getEncodedForm());
		else {
			final byte[] locInfo = new byte[18];
			for(int i = 0; i < 18; i++) locInfo[i] = (byte) 0xff;
			ausContextCache.setLocationInfo(locInfo);
		}
		
		final InapCallContext icc = new InapCallContext();
		CommonVariableHelper.loadCallContext(icc, idp.getDialog(), AusBaseSbb.this);
		CommonVariableHelper.loadCallContextCustomObjects(icc, idp.getDialog(), AusBaseSbb.this);
		CommonVariableHelper.loadCallContextServiceObjects(icc, idp.getDialog(), AusBaseSbb.this, AusSbb.SERVICE_NAME);
		
		final boolean inapParamsConsistencyRes = ausContextCache.processCallContext(icc, idpArg, AusBaseSbb.this);
		if(!inapParamsConsistencyRes)
			getAlarmFacility().raiseAlarm(
					String.format("%s.%s", AusSbb.TRACER_NAME, AusAlarms.INAP_INCONSISTENT_DATA.getAlarmType()),
					String.format("MSISDN:%s", ausContextCache.getMsisdn()),
					AusAlarms.INAP_INCONSISTENT_DATA.getAlarmLevel(),
					AusAlarms.INAP_INCONSISTENT_DATA.getFinalMessage());
		
		return inapParamsConsistencyRes;
	}
	
	protected CallConnectInParam prepareCallConnectParams(final ActivityContextInterface aci, final boolean isChargingEnabled) {
		final CallConnectInParam callConnectInParam = new CallConnectInParam();

		setCommonInParams(callConnectInParam, aci);
		
		callConnectInParam.setParentObject(null);
		
        callConnectInParam.getHuntingInfo().setNumberList(new String[] { getAusContextCache().getPrefixNumber().concat(getAusContextCache().getCalledNumber()) });
        callConnectInParam.getHuntingInfo().setMaxCallAttempts(1);
        callConnectInParam.getHuntingInfo().setNatureOfAddress(CalledPartyNumber.Nature.fromValue(getAusContextCache().getCalledNumberTON()));
        callConnectInParam.getHuntingInfo().setNumberingPlan(NumberingPlan.fromValue(getAusContextCache().getCalledNumberNumberingPlan()));
        callConnectInParam.getHuntingInfo().setRoutingToInternalNetworkNumber(CalledPartyNumber.RoutingToInternalNetworkNumber.fromValue(getAusContextCache().getCalledNumberRoutingToInternalNetwork()));
        callConnectInParam.getHuntingInfo().setResultExpected(true);
        
        //hunting pattern is currently not used by call connect SBB
        callConnectInParam.getHuntingInfo().setHuntingPattern(0);
        //hunting flag has to be set to false in order to send connect and do not arm any events
        callConnectInParam.setHuntingFlag(false);
        //inform the call connect SBB that the number to be connected is a directory number
        callConnectInParam.setList(false);

        if(isChargingEnabled)
        	callConnectInParam.setChargingInfo(prepareChargingParam(aci));
        
        if(isTraceable(TraceLevel.FINEST))
			finest(String.format("prepareCallConnectParams method: INAP CONNECT params are prepared: [%s]", callConnectInParam));

        return callConnectInParam;
	}
	
	private ChargingInParam prepareChargingParam(final ActivityContextInterface aci) {
        final ChargingInParam chargingInParam = new ChargingInParam();
        setCommonInParams(chargingInParam, aci);
        
        final CalledPartyNumber calledPartyNumberFormatted = new CalledPartyNumber(
        		CalledPartyNumber.Nature.fromValue(getAusContextCache().getCalledNumberTONFormatted()),
        		CalledPartyNumber.RoutingToInternalNetworkNumber.fromValue(getAusContextCache().getCalledNumberRoutingToInternalNetwork()),
        		NumberingPlan.fromValue(getAusContextCache().getCalledNumberNumberingPlan()),
        		getAusContextCache().getCalledNumberFormatted(),
        		CalledPartyNumber.ADDRESS_CODEC.getHexVariant());

        chargingInParam.setServiceType(getAusContextCache().getServiceIdentifier());
        chargingInParam.setSciEnabled(getAusContextCache().getScenario().isSciEnabled());
        chargingInParam.setFciEnabled(getAusContextCache().getScenario().isFciEnabled());
        chargingInParam.setSendingSide(true);
        chargingInParam.setBearerType(TeleserviceCode.SPEECH.getId());
        chargingInParam.setChargingBase(UANChargingGlobals.CH_SCPCONTROLLED);
        chargingInParam.setCallingPartyCategory(getAusContextCache().getCallingPartyCategory());
        chargingInParam.setBNumber(calledPartyNumberFormatted);
        chargingInParam.setLocInfo(getAusContextCache().getLocationInfo());

        if(getAusContextCache().getScenario().isSciEnabled())
        	chargingInParam.setSciId(getAusContextCache().getChargingSciId().toString());
        
        if(getAusContextCache().getScenario().isFciEnabled()) {
        	chargingInParam.setPartyToCharge(true);
        	chargingInParam.setFciId(getAusContextCache().getChargingFciId().toString());
        }

        return chargingInParam;
    }
	
	protected UIInParam prepareAnnouncementParams(final ActivityContextInterface aci) {
		final UIInParam annInParam = new UIInParam();
		setCommonInParams(annInParam, aci);
		
		final List<Integer> annIdList = new ArrayList<Integer>();
		annIdList.add(getAusContextCache().getAnnouncementId());

		annInParam.setAnncIdList(annIdList);
		annInParam.setInterruptible(false);
		annInParam.setSrfConnected(false);
		annInParam.setDisconnSRF(true);
		annInParam.setSrrRequired(true);
		annInParam.setCallBack((UANCallBackInterface) this.getSbbLocalObject());

		return annInParam;
	}
	
	private void setCommonInParams(final UanInParam param, final ActivityContextInterface aci) {
		param.setAci(aci);
		param.setEventLogId(AusSbb.TRACER_NAME);
		param.setTracerId(AusSbb.TRACER_NAME);
		param.setTraceLevel(getSubscriberTraceLevel().toInt());
		param.setSessionId(getSessionIdImpl());
		param.setServiceName(AusSbb.SERVICE_NAME);
		param.setSubSystemId(Integer.valueOf(AusEventCodes.SUBSYSTEM_IDENTIFIER * 100));
	}
	
	protected String prettyPrintCallConnectParams(final CallConnectInParam inParams) {
		final StringBuilder buff = new StringBuilder("call rerouting to [add,noa,np,rtin] [");
		buff.append(inParams.getHuntingInfo().getNumberList()[0]).append(",");
		buff.append(inParams.getHuntingInfo().getNatureOfAddress().intValue()).append(",");
		buff.append(inParams.getHuntingInfo().getNumberingPlan().intValue()).append(",");
		buff.append(inParams.getHuntingInfo().getRoutingToInternalNetworkNumber().intValue());
		buff.append("] with charging params [FciId,SciId] [");
		buff.append(inParams.getChargingInfo()!=null ? inParams.getChargingInfo().getFciId() : "empty").append(",");
		
		if(inParams.getChargingInfo() == null)
			buff.append("empty");
		else if(inParams.getChargingInfo().getSciId() != null)
			buff.append(inParams.getChargingInfo().getSciId());
		else		
			buff.append(getAusContextCache().getChargingSciId()); 
		buff.append("]");
		
		return buff.toString();
	}
	
	protected String getDialogStateFromAci(final ActivityContextInterface aci) {
		if(aci.getActivity() instanceof CCDialog)
			return ((CCDialog) aci.getActivity()).getDialogState().name();
		else
			return "UNSPECIFIED";
	}
	
	@Override
	public abstract void generateSessionId();
	
	/*****************************************************************************
	 ************** Methods impl enforced by TracerCallback **********************
	 *****************************************************************************/

    @Override
	public void tracef(final TraceLevel level, final String format, final Object... args) {
    	if(isTraceable(level))
    		doTracing(level, getSessionIdImpl(), String.format(format, args), null);
	}

	@Override
	public void tracef(final TraceLevel level, final Throwable t, final String format, final Object... args) {
		if(isTraceable(level))
			doTracing(level, getSessionIdImpl(), String.format(format, args), t);
	}

	@Override
	public void tracel(final TraceLevel level, final Object... messages) {
		if(isTraceable(level)) {
			if (messages.length == 1)
	            doTracing(level, getSessionIdImpl(), messages[0].toString(), null);
	        else
	            doTracing(level, getSessionIdImpl(), Arrays.toString(messages), null);
		}
	}
	
	
	/*****************************************************************************
	 ************** Inherited abstract methods from BaseSbb **********************
	 *****************************************************************************/

	/**
     * Used by the BaseSbb SBB to initiate main tracer.
     * @return String tracer name for the current SBB
     */
    @Override
    public String getTraceMessageType() {
        return AusSbb.TRACER_NAME;
    }
    
    /**
     * Used by child SBBs for raising alarms.
     * @return String identifier of the current SBB
     */
    @Override
	public String moduleName() {
    	return AusSbb.TRACER_NAME;
	}
    
    
	/*****************************************************************************
	 *********************** CMP fields declaration ******************************
	 *****************************************************************************/

	/**
     * An entire call context that contains params relevant to AUS service logic.
     * @return AusContext cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract AusContext getAusContext();
	
	/**
     * An entire call context that contains params relevant to AUS service logic.
     * @param ausCtx cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract void setAusContext(AusContext ausCtx);
	
	/**
     * Timer for guarding {@link AusSbbStateMachine.State#WAIT_FOR_ANN_COMPLETION} state.
     * @return TimerID cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract TimerID getAnnTimerId();
	
	/**
     * Timer for guarding {@link AusSbbStateMachine.State#WAIT_FOR_ANN_COMPLETION} state.
     * @param timerId cmp field value
     * 
     * @slee.cmp-method
     */
	public abstract void setAnnTimerId(TimerID timerId);
	
	/**
     * Getter for cmp field, must override a corresponding {@link com.nsn.gdo.feniks.slee.uan.common.BaseSbb#getSessionIdImpl()}
     * in order to make it visible for local XDoclet
     * @return String cmp field value
     * 
     * @slee.cmp-method
     */
	@Override
    public abstract SessionTraceContext getSessionIdImpl();
	
	/**
     * Setter for cmp field, must override a corresponding {@link com.nsn.gdo.feniks.slee.uan.common.BaseSbb#setSessionIdImpl(SessionTraceContext sessionID)}
     * in order to make it visible for local XDoclet 
     * @param sessionID cmp field value
     * 
     * @slee.cmp-method
     */
	@Override
	public abstract void setSessionIdImpl(SessionTraceContext sessionID);
	
	/**
     * Getter for cmp field, must override a corresponding {@link com.nsn.gdo.feniks.slee.uan.common.BaseSbb#getSubscriberTraceLevel()}
     * in order to make it visible for local XDoclet
     * @return String cmp field value
     * 
     * @slee.cmp-method
     */
	@Override
	public abstract TraceLevel getSubscriberTraceLevel();
	
	/**
     * Setter for cmp field, must override a corresponding {@link com.nsn.gdo.feniks.slee.uan.common.BaseSbb#setSubscriberTraceLevel(TraceLevel traceLevel)}
     * in order to make it visible for local XDoclet 
     * @param traceLevel cmp field value
     * 
     * @slee.cmp-method
     */
	@Override
	public abstract void setSubscriberTraceLevel(TraceLevel traceLevel);


	/*****************************************************************************
	 *************************** Child relations *********************************
	 *****************************************************************************/

	/**
	 * Create {@link CallConnectSbbLocalInterface} instance
	 *
	 * @return local interface to a given SBB instance
	 */
	CallConnectSbbLocalInterface getCallConnectSbb() throws Exception {
		if(isTraceable(FINEST))
			finest(String.format("getCallConnectSbb method: entering, current state is [%b,%b]", callConnectSbb==null, getCallConnectSbbRelation().isEmpty()));

		if (callConnectSbb == null) {
			if (getCallConnectSbbRelation().isEmpty())
				callConnectSbb = (CallConnectSbbLocalInterface) getCallConnectSbbRelation().create();
			else
				callConnectSbb = (CallConnectSbbLocalInterface) getCallConnectSbbRelation().iterator().next();
		}

		if (callConnectSbb == null)
			throw new CreateException("CallConnectSbb create method returned null result");

		return callConnectSbb;
	}
	
	/**
	 * Create {@link IVRInterfaceSbbLocalInterface} instance
	 *
	 * @return local interface to a given SBB instance
	 */
	IVRInterfaceSbbLocalInterface getUserInteractionSbb() throws Exception {
		if(isTraceable(FINEST))
			finest(String.format("getUserInteractionSbb method: entering, current state is [%b,%b]", userInteractionSbb==null, getUserInteractionSbbRelation().isEmpty()));

		if (userInteractionSbb == null) {
			if (getUserInteractionSbbRelation().isEmpty())
				userInteractionSbb = (IVRInterfaceSbbLocalInterface) getUserInteractionSbbRelation().create();
			else
				userInteractionSbb = (IVRInterfaceSbbLocalInterface) getUserInteractionSbbRelation().iterator().next();
		}

		if (userInteractionSbb == null)
			throw new CreateException("UserInteractionSbb create method returned null result");

		return userInteractionSbb;
	}

    /**
     * @slee.child-relation-method
     *   sbb-alias-ref="CallConnectSbb"
     *   default-priority="0"
     */
 	public abstract ChildRelation getCallConnectSbbRelation();
 	protected CallConnectSbbLocalInterface callConnectSbb = null;
 	
    /**
     * @slee.child-relation-method
     *   sbb-alias-ref="UserInteractionSbb"
     *   default-priority="0"
     */
 	public abstract ChildRelation getUserInteractionSbbRelation();
 	protected IVRInterfaceSbbLocalInterface userInteractionSbb = null;


 	/*****************************************************************************
	 ************************* Other SLEE related ********************************
	 *****************************************************************************/

	/**
	 * Aquires {@link AusUsageParameters} instance used for stats updates
	 * @return interface to usage params instance
	 */
	public abstract AusUsageParameters getDefaultSbbUsageParameterSet();
}
