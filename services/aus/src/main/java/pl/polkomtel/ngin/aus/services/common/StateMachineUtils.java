package pl.polkomtel.ngin.aus.services.common;

import pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs;

/**
 * Helper class used for FSM state machine operations that are not provided by default framework.
 * 
 * @author adam.tylman
 */
public class StateMachineUtils {

	/**
	 * Gets names of all inputs (state machine events) that are currently raised.
	 * 
	 * @param inputs MSISDN number 
	 * @return names of all inputs found
	 */
	public static String getInputsRaised(final Inputs inputs) {
		final StringBuilder builder = new StringBuilder("[");
		if(inputs.local.context_creation_failed.isRaised())
			builder.append(inputs.local.context_creation_failed.name()).append(",");
		if(inputs.ssp.CUE_sent.isRaised())
			builder.append(inputs.ssp.CUE_sent.name()).append(",");
		if(inputs.ssp.CUE_failed.isRaised())
			builder.append(inputs.ssp.CUE_failed.name()).append(",");
		if(inputs.local.context_creation_success.isRaised())
			builder.append(inputs.local.context_creation_success.name()).append(",");
		if(inputs.cupr.service_details_found.isRaised())
			builder.append(inputs.cupr.service_details_found.name()).append(",");
		if(inputs.local.call_blocking_enabled.isRaised())
			builder.append(inputs.local.call_blocking_enabled.name()).append(",");
		if(inputs.local.call_rerouting_enabled.isRaised())
			builder.append(inputs.local.call_rerouting_enabled.name()).append(",");
		if(inputs.local.charging_disabled.isRaised())
			builder.append(inputs.local.charging_disabled.name()).append(",");
		if(inputs.local.fci_enabled.isRaised())
			builder.append(inputs.local.fci_enabled.name()).append(",");
		if(inputs.local.sci_enabled.isRaised())
			builder.append(inputs.local.sci_enabled.name()).append(",");
		if(inputs.local.internal_error.isRaised())
			builder.append(inputs.local.internal_error.name()).append(",");
		if(inputs.cupr.ldap_error.isRaised())
			builder.append(inputs.cupr.ldap_error.name()).append(",");
		if(inputs.ssp.rerouting_error.isRaised())
			builder.append(inputs.ssp.rerouting_error.name()).append(",");
		if(inputs.ssp.charging_error.isRaised())
			builder.append(inputs.ssp.charging_error.name()).append(",");
		if(inputs.ssp.ann_error.isRaised())
			builder.append(inputs.ssp.ann_error.name()).append(",");
		if(inputs.ssp.call_completed.isRaised())
			builder.append(inputs.ssp.call_completed.name()).append(",");
		if(inputs.ssp.wait_for_announcement_completion.isRaised())
			builder.append(inputs.ssp.wait_for_announcement_completion.name()).append(",");
		if(inputs.ssp.ann_played.isRaised())
			builder.append(inputs.ssp.ann_played.name()).append(",");
		if(inputs.ssp.ann_failed.isRaised())
			builder.append(inputs.ssp.ann_failed.name()).append(",");
		if(inputs.ssp.ann_timeout.isRaised())
			builder.append(inputs.ssp.ann_timeout.name()).append(",");
		if(inputs.ssp.REL_failed.isRaised())
			builder.append(inputs.ssp.REL_failed.name()).append(",");
		if(inputs.ssp.user_abort_received.isRaised())
			builder.append(inputs.ssp.user_abort_received.name()).append(",");
		if(inputs.ssp.provider_abort_received.isRaised())
			builder.append(inputs.ssp.provider_abort_received.name()).append(",");

		if(builder.length()>1)
			return builder.deleteCharAt(builder.length()-1).append("]").toString();
		else
			return "UNSPECIFIED";
	}
}
