package pl.polkomtel.ngin.aus.services.event;

import com.nsn.ploc.feniks.slee.nsnbaselib.log.EventCode;

/**
 * Enables helper classes to dump EDRs through a specific SBB object implementing such an interface.
 * 
 * @author adam.tylman
 */
public interface EventReporterCallback {
	
	/**
	 * Dumps a single EDR with a specific event code.
	 * @param eventCode event code indicator  
     */
	void publishReportEvent(EventCode eventCode);
	
	/**
	 * Dumps a single EDR together with additional and more extensive info.
	 * @param eventCode event code indicator
	 * @param errorCause should appear in case of an error/exception thrown
	 * @param additionalInfo various additional info to attach to the EDR
	 */
	void publishReportEvent(EventCode eventCode, String errorCause, String additionalInfo);
}