package pl.polkomtel.ngin.aus.services.common;

import static com.nsn.ploc.feniks.slee.nsnbaselib.consts.InapCallContextConsts.ORIGINAL_CALLED_PARTY_NATURE;
import static com.nsn.ploc.feniks.slee.nsnbaselib.consts.InapCallContextConsts.ORIGINAL_CALLED_PARTY_NUMBER;
import static com.nsn.ploc.feniks.slee.nsnbaselib.consts.InapCallContextConsts.ORIGINAL_CALLING_PARTY_NATURE;
import static com.nsn.ploc.feniks.slee.nsnbaselib.consts.InapCallContextConsts.ORIGINAL_CALLING_PARTY_NUMBER;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

import javax.slee.facilities.TraceLevel;

import org.apache.commons.lang.StringUtils;

import com.nsn.ploc.feniks.slee.nsnbaselib.InapCallContext;
import com.nsn.ploc.feniks.slee.nsnbaselib.consts.TeleserviceCode;
import com.nsn.ploc.feniks.slee.nsnbaselib.log.TracerCallback;
import com.opencloud.slee.resources.cgin.etsi_inap_cs1.CS1ExtensionField;
import com.opencloud.slee.resources.cgin.etsi_inap_cs1.CS1InitialDPArg;
import com.opencloud.slee.resources.in.datatypes.cc.CalledPartyNumber;
import com.opencloud.slee.resources.in.datatypes.cc.CallingPartyNumber;
import com.opencloud.slee.resources.in.datatypes.cc.CallingPartysCategory;
import com.opencloud.slee.resources.in.datatypes.cc.IMSIAddress;
import com.opencloud.util.FastSerializable;

/**
 * Stores all service execution context variables including: call context args that are passed down by SIS and service config
 * fetched from CUPR.
 * 
 * @author adam.tylman
 */
public final class AusContext extends FastSerializableEntity implements FastSerializable {

	static final long serialVersionUID = -4318761347157961648L;
	
	//Service logic execution outputs
	private long startTime = 0;
	private AusScenarioType scenario = AusScenarioType.UNSPECIFIED;

	//Parameters retrieved from SIS context based on IDP event
    private String calledNumber = "";
    private int calledNumberTON = -1;
    private int calledNumberNumberingPlan = -1;
    private int calledNumberRoutingToInternalNetwork = -1;
    private String calledNumberFormatted = "";
    private int calledNumberTONFormatted = -1;
    
    private String callingNumber = "";
    private int callingNumberTON = -1;
    private byte callingPartyCategory = (byte) -1;
    private String callingNumberFormatted = "";
    private int callingNumberTONFormatted = -1;

    private String redirectingNumberFormatted = "";
    private int redirectingNumberTONFormatted = -1;
    
    private int serviceKey = -1;
    private String imsi = "";	
    private boolean callForwarded = false;
    private TeleserviceCode teleservice = TeleserviceCode.ALL;
    private byte[] locationInfo = null;
    
    /*
     * AUS profile from CUPR (SciId is PK of COMMON.TARIFF_DEF, FciId is PK of UAN.UAN_FCI, AnnId is PK of COMMON.ANNOUNCEMENT)
     * Initially service definition details were retrieved by SIS (SERVICE_IDENTIFIER column from PROFILES.SERVICES table)
     * but now it has to be fetched from CUPR because it's not available in script vars anymore.
     */
    private String msisdn = null;
    private Boolean isBlocked = null;
    private String prefixNumber = null;
    private Integer announcementId = null;
    private Integer chargingSciId = null;
    private Integer chargingFciId = null;
    private String serviceIdentifier = null;
    
    /**
	 * Default public constructor.
	 */
    public AusContext() {
    	startTime = System.currentTimeMillis();
    }
	
    /**
     * Instantiates all local variables that refer to INAP call context.
     * @param icc a complete call context which keeps all parsed and already formated args
     * @param idpArg pure IDP argument that is used if there are some args missing in a/m call context
     * @param callback callback interface to the parent SBB used for tracing
     * @return true if all mandatory fields were found, false otherwise
	 */
	public boolean processCallContext(final InapCallContext icc, final CS1InitialDPArg idpArg, final TracerCallback callback) {
		if(callback.isTraceable(TraceLevel.FINEST))
			callback.tracef(TraceLevel.FINEST, "processCallContext method: NOKIA INAP context to process:\n" + icc.dump());
		
		msisdn = icc.getCalledPartyNumber();

		calledNumber = (String) icc.getCustomObject(ORIGINAL_CALLED_PARTY_NUMBER);
		calledNumberTON = ((CalledPartyNumber.Nature) icc.getCustomObject(ORIGINAL_CALLED_PARTY_NATURE)).intValue();
		calledNumberNumberingPlan = idpArg.getCalledPartyNumber().getNumberingPlan().intValue();
		calledNumberRoutingToInternalNetwork = idpArg.getCalledPartyNumber().getRoutingToInternalNetworkNumber().intValue();
		calledNumberFormatted = icc.getCalledPartyNumber();
		calledNumberTONFormatted = icc.getCalledPartyNoA().intValue();

		callingNumber = (String) icc.getCustomObject(ORIGINAL_CALLING_PARTY_NUMBER);
		callingNumberTON = ((CallingPartyNumber.Nature) icc.getCustomObject(ORIGINAL_CALLING_PARTY_NATURE)).intValue();
		callingNumberFormatted = icc.getCallingPartyNumber();
		callingNumberTONFormatted = icc.getCallingPartyNoA().intValue();
		
		callForwarded = icc.isCallForwarded();
		if(callForwarded) {
			redirectingNumberFormatted = icc.getRedirectingPartyAddress();
			redirectingNumberTONFormatted = icc.getRedirectingPartyAddressNoA().intValue();
		}

		if(idpArg.hasCallingPartysCategory())
			callingPartyCategory = idpArg.getCallingPartysCategory().getCategory().byteValue();
		else
			callingPartyCategory = CallingPartysCategory.Category.UNKNOWN.byteValue();

		if (icc.isServiceKeyPresent())
			serviceKey = icc.getServiceKey();

		if(icc.isVideoCall())
			teleservice = TeleserviceCode.VIDEO;
		else
			teleservice = icc.getTeleservice();
		
		if(idpArg.hasExtensions()) {
			final CS1ExtensionField[] extFields = idpArg.getExtensions();
			for (final CS1ExtensionField extField : extFields) {
				if (extField.getType().getLocal() == 26) {
					imsi = ((IMSIAddress) extField.getValue().getContainedValue()).getAddress().toUpperCase();
					break;
				}
	        }
		}

		return validateInapParamsBasicConsistency(callback);
	}
	
	/**
     * Verifies if basic fields from INAP call context are present from the service logic execution point of view.
     * @param callback callback interface to the parent SBB used for tracing
     * @return true if all fields were found, false otherwise
	 */
	public boolean validateInapParamsBasicConsistency(final TracerCallback callback) {
		final boolean sisContextPrerequisites =
				StringUtils.isNotBlank(calledNumber)
				&& (calledNumberTON>=0)
				&& StringUtils.isNotBlank(callingNumberFormatted)
				&& StringUtils.isNotBlank(calledNumberFormatted)
				&& (calledNumberTONFormatted>=0)
				&& (calledNumberNumberingPlan>=0)
				&& (calledNumberRoutingToInternalNetwork>=0);

		if(!sisContextPrerequisites)
			callback.tracef(
					TraceLevel.WARNING,
					"validateInapParamsConsistency method: missing mandatory parameters, result for [sisContext] group of prerequisites is [%b]",
					sisContextPrerequisites);

		return sisContextPrerequisites;
	}
	
    /**
     * Verifies if all mandatory fields from INAP call context are present from the service logic execution point of view.
     * @param callback callback interface to the parent SBB used for tracing
     * @return true if all mandatory fields were found, false otherwise
	 */
	public boolean validateInapParamsExtConsistency(final TracerCallback callback) {
		final boolean generalPrerequisites =
				(teleservice.equals(TeleserviceCode.SPEECH) || teleservice.equals(TeleserviceCode.ALL))
				&& StringUtils.isNotBlank(msisdn);

		final boolean chargingPrerequisites = (callingPartyCategory >= 0) && (calledNumberTONFormatted >= 0);

		final boolean chargingFciPrerequisites = (locationInfo!=null) && locationInfo.length>0;

		final boolean res = generalPrerequisites && chargingPrerequisites && chargingFciPrerequisites;

		if(!res)
			callback.tracef(
					TraceLevel.WARNING,
					"validateInapParamsConsistency method: missing mandatory parameters, result for [general,charging,fci] groups of prerequisites is [%b,%b,%b]",
					generalPrerequisites, chargingPrerequisites, chargingFciPrerequisites);

		return res;
	}
	
    /**
     * Verifies if all mandatory service config parameters fetched from CUPR are present.
     * @return true if all mandatory fields were found, false otherwise
	 */
	public boolean validateLdapParamsConsistency() {
		if(isBlocked == null)
			return false;
		else if(isBlocked.booleanValue() && announcementId == null)
			return false;
		else if(!isBlocked.booleanValue() && StringUtils.isBlank(prefixNumber))
			return false;
		else if(!isBlocked.booleanValue() && StringUtils.isNotBlank(prefixNumber) && chargingFciId != null && StringUtils.isBlank(serviceIdentifier))
			return false;
		else				
			return true;
	}
	
	/**
     * Determines which scenario to execute on the basis of service profile fetched from CUPR and sets the corresponding
     * local variable {@link AusContext#scenario} to the proper value.
	 */
	public void computeScenarioType() {
		if(isBlocked.booleanValue())
			scenario = AusScenarioType.BLOCKING;
		else if(chargingFciId != null && chargingSciId != null)
			scenario = AusScenarioType.REROUTING_WITH_FCI_SCI;
		else if(chargingFciId != null)
			scenario = AusScenarioType.REROUTING_WITH_FCI_ONLY;
		else if(chargingSciId != null) 
			scenario = AusScenarioType.REROUTING_WITH_SCI_ONLY;
		else
			scenario = AusScenarioType.DIRECT_REROUTING;
	}
	
	/******************************************************************************
	 ********************** Local variables accessors *****************************
	 ******************************************************************************/
	
	/**
	 * @return the startTime
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * @return the scenario
	 */
	public AusScenarioType getScenario() {
		return scenario;
	}

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}
	
	/**
	 * @return the isBlocked
	 */
	public Boolean getIsBlocked() {
		return isBlocked;
	}

	/**
	 * @param isBlocked the isBlocked to set
	 */
	public void setIsBlocked(final Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	
	/**
	 * @return the prefixNumber
	 */
	public String getPrefixNumber() {
		return prefixNumber;
	}
	
	/**
	 * @param prefixNumber the prefixNumber to set
	 */
	public void setPrefixNumber(final String prefixNumber) {
		this.prefixNumber = prefixNumber;
	}
	
	/**
	 * @return the announcementId
	 */
	public Integer getAnnouncementId() {
		return announcementId;
	}
	
	/**
	 * @param announcementId the announcementId to set
	 */
	public void setAnnouncementId(final Integer announcementId) {
		this.announcementId = announcementId;
	}
	
	/**
	 * @return the chargingFciInParam
	 */
	public Integer getChargingFciId() {
		return chargingFciId;
	}
	
	/**
	 * @param chargingFciId the chargingFciInParam to set
	 */
	public void setChargingFciId(final Integer chargingFciId) {
		this.chargingFciId = chargingFciId;
	}
	
	/**
	 * @return the serviceIdentifier
	 */
	public String getServiceIdentifier() {
		return serviceIdentifier;
	}
	
	/**
	 * @param serviceIdentifier the serviceIdentifier to set
	 */
	public void setServiceIdentifier(final String serviceIdentifier) {
		this.serviceIdentifier = serviceIdentifier;
	}

	/**
	 * @return the chargingSciMczParam
	 */
	public Integer getChargingSciId() {
		return chargingSciId;
	}
	
	/**
	 * @param chargingSciId the chargingSciMczParam to set
	 */
	public void setChargingSciId(final Integer chargingSciId) {
		this.chargingSciId = chargingSciId;
	}
	
	/**
	 * @return the calledNumberFormatted
	 */
	public String getCalledNumber() {
		return calledNumber;
	}

	/**
	 * @return the calledNumberFormatted
	 */
	public int getCalledNumberTON() {
		return calledNumberTON;
	}

	/**
	 * @return the calledNumberNumberingPlan
	 */
	public int getCalledNumberNumberingPlan() {
		return calledNumberNumberingPlan;
	}

	/**
	 * @return the calledNumberRoutingToInternalNetwork
	 */
	public int getCalledNumberRoutingToInternalNetwork() {
		return calledNumberRoutingToInternalNetwork;
	}

	/**
	 * @return the calledNumberFormatted
	 */
	public String getCalledNumberFormatted() {
		return calledNumberFormatted;
	}

	/**
	 * @return the callingPartyCategory
	 */
	public byte getCallingPartyCategory() {
		return callingPartyCategory;
	}

	/**
	 * @return the callingNumberTONFormatted
	 */
	public int getCallingNumberTONFormatted() {
		return callingNumberTONFormatted;
	}

	/**
	 * @return the callingNumberFormatted
	 */
	public String getCallingNumberFormatted() {
		return callingNumberFormatted;
	}

	/**
	 * @return the calledNumberTONFormatted
	 */
	public int getCalledNumberTONFormatted() {
		return calledNumberTONFormatted;
	}

	/**
	 * @return the redirectingNumberFormatted
	 */
	public String getRedirectingNumberFormatted() {
		return redirectingNumberFormatted;
	}

	/**
	 * @return the redirectingNumberTONFormatted
	 */
	public int getRedirectingNumberTONFormatted() {
		return redirectingNumberTONFormatted;
	}

	/**
	 * @return the serviceKey
	 */
	public int getServiceKey() {
		return serviceKey;
	}

	/**
	 * @return the locationInfo
	 */
	public byte[] getLocationInfo() {
		return locationInfo.clone();
	}

	/**
	 * @param locationInfo the locationInfo to set
	 */
	public void setLocationInfo(final byte[] locationInfo) {
		this.locationInfo = locationInfo!=null ? locationInfo.clone() : null;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("AusContext [startTime=");
		builder.append(startTime);
		builder.append(", ");
		if (scenario != null) {
			builder.append("scenario=");
			builder.append(scenario);
			builder.append(", ");
		}
		if (serviceIdentifier != null) {
			builder.append("serviceIdentifier=");
			builder.append(serviceIdentifier);
			builder.append(", ");
		}
		if (calledNumber != null) {
			builder.append("calledNumber=");
			builder.append(calledNumber);
			builder.append(", ");
		}
		builder.append("calledNumberTON=");
		builder.append(calledNumberTON);
		builder.append(", calledNumberNumberingPlan=");
		builder.append(calledNumberNumberingPlan);
		builder.append(", calledNumberRoutingToInternalNetwork=");
		builder.append(calledNumberRoutingToInternalNetwork);
		builder.append(", ");
		if (calledNumberFormatted != null) {
			builder.append("calledNumberFormatted=");
			builder.append(calledNumberFormatted);
			builder.append(", ");
		}
		builder.append("calledNumberTONFormatted=");
		builder.append(calledNumberTONFormatted);
		builder.append(", ");
		if (callingNumber != null) {
			builder.append("callingNumber=");
			builder.append(callingNumber);
			builder.append(", ");
		}
		builder.append("callingNumberTON=");
		builder.append(callingNumberTON);
		builder.append(", callingPartyCategory=");
		builder.append(callingPartyCategory);
		builder.append(", ");
		if (callingNumberFormatted != null) {
			builder.append("callingNumberFormatted=");
			builder.append(callingNumberFormatted);
			builder.append(", ");
		}
		builder.append("callingNumberTONFormatted=");
		builder.append(callingNumberTONFormatted);
		builder.append(", ");
		if (redirectingNumberFormatted != null) {
			builder.append("redirectingNumberFormatted=");
			builder.append(redirectingNumberFormatted);
			builder.append(", ");
		}
		builder.append("redirectingNumberTONFormatted=");
		builder.append(redirectingNumberTONFormatted);
		builder.append(", serviceKey=");
		builder.append(serviceKey);
		builder.append(", ");
		if (imsi != null) {
			builder.append("imsi=");
			builder.append(imsi);
			builder.append(", ");
		}
		builder.append("callForwarded=");
		builder.append(callForwarded);
		builder.append(", ");
		if (teleservice != null) {
			builder.append("teleservice=");
			builder.append(teleservice);
			builder.append(", ");
		}
		if (locationInfo != null) {
			builder.append("locationInfo=");
			builder.append(Arrays.toString(locationInfo));
			builder.append(", ");
		}
		if (msisdn != null) {
			builder.append("msisdn=");
			builder.append(msisdn);
			builder.append(", ");
		}
		if (isBlocked != null) {
			builder.append("isBlocked=");
			builder.append(isBlocked);
			builder.append(", ");
		}
		if (prefixNumber != null) {
			builder.append("prefixNumber=");
			builder.append(prefixNumber);
			builder.append(", ");
		}
		if (announcementId != null) {
			builder.append("announcementId=");
			builder.append(announcementId);
			builder.append(", ");
		}
		if (chargingFciId != null) {
			builder.append("chargingFciId=");
			builder.append(chargingFciId);
			builder.append(", ");
		}
		if (serviceIdentifier != null) {
			builder.append("serviceIdentifier=");
			builder.append(serviceIdentifier);
			builder.append(", ");
		}
		if (chargingSciId != null) {
			builder.append("chargingSciId=");
			builder.append(chargingSciId);
		}
		builder.append("]");
		return builder.toString();
	}

	/*****************************************************************************
	 ************ Methods used for serialization/deserialization *****************
	 *****************************************************************************/
	
	/**
	 * Enforced by {@link FastSerializable}.
	 * @param in the input stream
	 * @throws IOException if an error occurs during serialization
	 */
	public AusContext(final DataInput in) throws IOException {
		startTime = in.readLong();
		scenario = AusScenarioType.fromNumeral(in.readInt());
		serviceIdentifier = readString(in);
		calledNumber = readString(in);
		calledNumberTON = in.readInt();
		calledNumberNumberingPlan = in.readInt();
		calledNumberRoutingToInternalNetwork = in.readInt();
		calledNumberFormatted = readString(in);
		calledNumberTONFormatted = in.readInt();
		callingNumber = readString(in);
		callingNumberTON = in.readInt();
		callingPartyCategory = in.readByte();
		callingNumberFormatted = readString(in);
		callingNumberTONFormatted = in.readInt();
		redirectingNumberFormatted = readString(in);
		redirectingNumberTONFormatted = in.readInt();
		serviceKey = in.readInt();
		imsi = readString(in);
		callForwarded = in.readBoolean();
		teleservice = TeleserviceCode.fromId(in.readInt());
		msisdn = readString(in);
		isBlocked = readBoolean(in);
		prefixNumber = readString(in);
		announcementId = readInteger(in);
		chargingFciId = readInteger(in);
		chargingSciId = readInteger(in);
		locationInfo = new byte[in.readByte()];
		in.readFully(locationInfo);
	}

	/**
	 * Enforced by {@link FastSerializable}.
	 * @param out the stream to serialize to
	 */
	@Override
	public void toStream(final DataOutput out) throws IOException {
		out.writeLong(startTime);
		out.writeInt(scenario.getNumeral());
		writeString(out, serviceIdentifier);
		writeString(out, calledNumber);
		out.writeInt(calledNumberTON);
		out.writeInt(calledNumberNumberingPlan);
		out.writeInt(calledNumberRoutingToInternalNetwork);
		writeString(out, calledNumberFormatted);
		out.writeInt(calledNumberTONFormatted);
		writeString(out, callingNumber);
		out.writeInt(callingNumberTON);
		out.writeByte(callingPartyCategory);
		writeString(out, callingNumberFormatted);
		out.writeInt(callingNumberTONFormatted);
		writeString(out, redirectingNumberFormatted);
		out.writeInt(redirectingNumberTONFormatted);
		out.writeInt(serviceKey);
		writeString(out, imsi);
		out.writeBoolean(callForwarded);
		out.writeInt(teleservice.getId());
		writeString(out, msisdn);
		writeBoolean(out, isBlocked);
		writeString(out, prefixNumber);
		writeInteger(out, announcementId);
		writeInteger(out, chargingFciId);
		writeInteger(out, chargingSciId);
	    out.writeByte(locationInfo.length);
	    out.write(locationInfo);
	}
}
