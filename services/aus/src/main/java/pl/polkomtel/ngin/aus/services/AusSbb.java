package pl.polkomtel.ngin.aus.services;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.SbbContext;
import javax.slee.facilities.TimerEvent;
import javax.slee.facilities.TimerID;
import javax.slee.facilities.TimerOptions;
import javax.slee.facilities.TraceLevel;

import pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions;
import pl.polkomtel.ngin.aus.services.alarm.AusAlarms;
import pl.polkomtel.ngin.aus.services.common.LdapUtils;
import pl.polkomtel.ngin.aus.services.common.StateMachineUtils;
import pl.polkomtel.ngin.aus.services.event.AusEventCodes;
import pl.polkomtel.ngin.aus.services.event.AusEventHelper;

import com.nsn.gdo.feniks.slee.uan.callconnect.CallConnectSbbLocalInterface;
import com.nsn.gdo.feniks.slee.uan.common.CallConnectGlobals;
import com.nsn.gdo.feniks.slee.uan.common.UIGlobals;
import com.nsn.gdo.feniks.slee.uan.common.localinterfaceargs.CallConnectInParam;
import com.nsn.gdo.feniks.slee.uan.common.localinterfaceargs.CallConnectOutParam;
import com.nsn.gdo.feniks.slee.uan.common.localinterfaceargs.UIInParam;
import com.nsn.gdo.feniks.slee.uan.common.localinterfaceargs.UIOutParam;
import com.nsn.gdo.feniks.slee.uan.userinteraction.IVRInterfaceSbbLocalInterface;
import com.nsn.ploc.feniks.slee.nsnbaselib.log.EventCode;
import com.nsn.ploc.feniks.slee.nsnbaselib.log.EventHelper;
import com.nsn.ploc.feniks.slee.nsnbaselib.log.EventRecordException;
import com.nsn.ploc.feniks.slee.resources.eventreporter.EventRecord;
import com.nsn.ploc.feniks.slee.resources.eventreporter.EventReporterProvider;
import com.opencloud.sce.fsmtool.FSMInput;
import com.opencloud.sce.fsmtool.Facilities;
import com.opencloud.sce.fsmtool.InputScheduler;
import com.opencloud.slee.resources.cgin.Dialog;
import com.opencloud.slee.resources.cgin.ProtocolException;
import com.opencloud.slee.resources.cgin.callcontrol.CCDialog;
import com.opencloud.slee.resources.cgin.callcontrol.CCReleaseCallArg;
import com.opencloud.slee.resources.in.datatypes.cc.Cause;
import com.opencloud.slee.resources.ldap.LDAPActivityContextInterfaceFactory;
import com.opencloud.slee.resources.ldap.LDAPProvider;
import com.opencloud.slee.resources.ldap.objects.ConnectionException;
import com.opencloud.slee.resources.ldap.objects.SearchException;

/**
 * @slee.service
 *   name="${project.root.name}"
 *   vendor="${aus.slee.vendor}"
 *   version="${aus.slee.version}"
 *   default-priority="0"
 * @slee.sbb
 *   id="${project.root.name}-sbb"
 *   name="${project.root.name}"
 *   vendor="${aus.slee.vendor}"
 *   version="${aus.slee.version}"
 *   reentrant="True"
 *   sbb-local-interface-name="com.nsn.gdo.feniks.slee.uan.common.callback.UANCallBackInterface"
 *   sbb-local-interface-description="Required by User Interaction child SBB in order to revert back to parent SBB upon play ann completion"
 *   sbb-usage-parameters-interface-name="pl.polkomtel.ngin.aus.services.stats.AusUsageParameters"
 * @slee.sbb-ref
 *   sbb-name="UanCallConnect"
 *   sbb-vendor="NSN"
 *   sbb-version="${uan.slee.version}"
 *   sbb-alias="CallConnectSbb"
 * @slee.sbb-ref
 *   sbb-name="UanUserInteraction"
 *   sbb-vendor="NSN"
 *   sbb-version="${uan.slee.version}"
 *   sbb-alias="UserInteractionSbb"
 * @slee.library-ref
 *   library-name="Base"
 *   library-vendor="NSN"
 *   library-version="${base.slee.version}"
 * @slee.library-ref
 *   library-name="FSMTool Library"
 *   library-vendor="OpenCloud"
 *   library-version="${fsm.slee.version}"
 * @slee.library-ref
 *   library-name="UanCommon"
 *   library-vendor="NSN"
 *   library-version="${uan.slee.version}"
 * @slee.library-ref
 *   library-name="UanUtil"
 *   library-vendor="NSN"
 *   library-version="${uan.slee.version}"
 * @slee.library-ref
 *   library-name="BaseSis"
 *   library-vendor="NSN"
 *   library-version="${basesis.slee.version}"
 * @slee.library-ref
 *   library-name="LDAP"
 *   library-vendor="OpenCloud"
 *   library-version="${ldap.slee.version}"
 * @slee.env-entry
 *   env-entry-name="DataBaseError"
 *   env-entry-type="java.lang.String"
 *   env-entry-value="DB_ERROR"
 * @slee.env-entry
 *   env-entry-name="RoutingConfigurationError"
 *   env-entry-type="java.lang.String"
 *   env-entry-value="ROUTING_CONF_ERROR"
* @slee.env-entry
 *   env-entry-name="ScreeningConfigurationError"
 *   env-entry-type="java.lang.String"
 *   env-entry-value="SCREENING_CONF_ERROR"
* @slee.env-entry
 *   env-entry-name="ChargingConfigurationError"
 *   env-entry-type="java.lang.String"
 *   env-entry-value="CHARGING_CONF_ERROR"
* @slee.env-entry
 *   env-entry-name="ServiceConfigurationError"
 *   env-entry-type="java.lang.String"
 *   env-entry-value="SERVICE_CONF_ERROR"
 * @slee.ra-type-binding
 *   resource-adaptor-type-name="EventReporter"
 *   resource-adaptor-type-vendor="NSN"
 *   resource-adaptor-type-version="${eventreporter.slee.version}"
 *   resource-adaptor-object-name="slee/resources/er/provider"
 *   resource-adaptor-entity-link="${eventreporter.slee.entity.link}"
 * @slee.ra-type-binding
 *   resource-adaptor-type-name="LDAP"
 *   resource-adaptor-type-vendor="OpenCloud"
 *   resource-adaptor-type-version="${ldap.slee.version}"
 *   activity-context-interface-factory-name="slee/resources/ldap/acifactory"
 *   resource-adaptor-object-name="slee/resources/ldap/provider"
 *   resource-adaptor-entity-link="${ldap.slee.entity.link}"  
 */
public abstract class AusSbb extends AusEventHandler implements AusSbbActions {

	public static final String TRACER_NAME = "plk.aus";
	static final String SERVICE_NAME = "AUS";
	static final long ANN_TIMEOUT = 90000;
	
	public static final String LDAP_PROVIDER_JNDI_NAME = "slee/resources/ldap/provider";
	private static final String LDAP_ACIF_JNDI_NAME = "slee/resources/ldap/acifactory";
	private static final String ER_PROVIDER_JNDI_NAME = "slee/resources/er/provider";
	
	private EventReporterProvider eventReportProvider = null;
	private LDAPProvider ldapProvider = null;
	private LDAPActivityContextInterfaceFactory ldapAciFactory = null;
	
	
	/*****************************************************************************
	 ********************* SLEE SBB lifecycle methods ****************************
	 *****************************************************************************/
	
	/**
     * Automatically called by SLEE upon SBB object creation.
     */
	@Override
	public void setSbbContext(final SbbContext context) {
		super.setSbbContext(context);
		
		try {
			final Context env = (Context) new InitialContext().lookup("java:comp/env");
			eventReportProvider = (EventReporterProvider) env.lookup(ER_PROVIDER_JNDI_NAME);
			ldapProvider = (LDAPProvider) env.lookup(LDAP_PROVIDER_JNDI_NAME);
			ldapAciFactory = (LDAPActivityContextInterfaceFactory) env.lookup(LDAP_ACIF_JNDI_NAME);
		} catch (final NamingException ne) {
			final String msg = "Error while getting environment entries or reference to environment: "+ne.getMessage();
			getSbbTracer().trace(TraceLevel.SEVERE, msg, ne);
			throw new RuntimeException(msg, ne);
		}
		
		registerActionImplementation(this);
	}
	
	/**
     * Automatically called by SLEE upon SBB entity creation.
     */
	@Override
	public void sbbPostCreate() throws CreateException {
		if(isTraceable(TraceLevel.FINEST))
			finest("Invoking lifecycle method: sbbPostCreate");
		super.sbbPostCreate();
		cleanNonCmpFields();
	}
	
	/**
     * Automatically called by SLEE upon SBB entity assignment.
     */
	@Override
	public void sbbActivate() {
		if(isTraceable(TraceLevel.FINEST))
			getSbbTracer().trace(TraceLevel.FINEST, "Invoking lifecycle method: sbbActivate");
		super.sbbActivate();
		cleanNonCmpFields();
	}
	
	/**
     * Automatically called by SLEE upon SBB object synchronization.
     */
	@Override
	public void sbbLoad() {
		if(isTraceable(TraceLevel.FINEST))
			finest("Invoking lifecycle method: sbbLoad");
		super.sbbLoad();
		cleanNonCmpFields();
	}
	
	private void cleanNonCmpFields() {
		callConnectSbb = null;
		userInteractionSbb = null;
		ausContextCache = null;
	}
	
	
	/*****************************************************************************
	 *************************** Helper methods **********************************
	 *****************************************************************************/

	@Override
	public void generateSessionId() {
		setSessionIdImpl(eventReportProvider.generateSessionTraceContext(TRACER_NAME));
	}
	
	@Override
	public void publishReportEvent(final EventCode eventCode, final String errorCause, final String additionalInfo) {
		try {
			final EventHelper eventHelper = new AusEventHelper(getAusContextCache(), errorCause, additionalInfo);
			if (eventReportProvider.isPublishingEnabled(getSessionIdImpl().getTracerName())) {
				final EventRecord eventRecord = new EventRecord(getSessionIdImpl(), eventCode.getEventCode());
				for (final EventRecord.EventRecordField field : eventHelper.getEventRecordFields(eventCode))
					eventRecord.add(field);
				eventReportProvider.publishEvent(eventRecord);
			} else
				warn(String.format("publishReportEvent method: publishing is disabled for AUS tracer [%s], please investigate!!", TRACER_NAME));
		} catch (final EventRecordException ex) {
			warn(String.format("publishReportEvent method: a general error for EDR reporting occurred for event code [%d]", eventCode.getEventCode()), ex);
		}
	}
	
	@Override
	public void publishReportEvent(final EventCode eventCode) {
		publishReportEvent(eventCode, "", "");
	}
	
	private void processCallConnectResult(final int result, final CallConnectInParam inParams, final CallConnectOutParam outParams, final Inputs inputs, final InputScheduler<FSMInput> inputScheduler) {	
		getDefaultSbbUsageParameterSet().incrementConnectAttempts(1L);
		if(isTraceable(TraceLevel.FINEST))
			finest(String.format("processCallConnectResult method: call connect module result [%d] and output [%s]", result, outParams.toString()));

		switch (result) {
			case CallConnectGlobals.CALL_CONNECT_OK:
			case CallConnectGlobals.CALL_CONNECT_DIRECT_CALL:
				if(isTraceable(TraceLevel.INFO))
					info(String.format("processCallConnectResult method: %s succeeded, call connect module result code is [%d]",
									prettyPrintCallConnectParams(inParams), result));
				inputScheduler.raise(inputs.ssp.call_completed);
				break;
			case CallConnectGlobals.CALL_CONNECT_OBJECT_NULL:
			case CallConnectGlobals.CALL_CONNECT_LIST_NULL:
			case CallConnectGlobals.CALL_CONNECT_INCORRECT_PARAMETER:
			case CallConnectGlobals.CALL_CONNECT_ERROR_EVENT:
				severe(String.format("processCallConnectResult method: a general error returned by call connect module [%d] for %s", result, prettyPrintCallConnectParams(inParams)));
				inputScheduler.raise(inputs.ssp.rerouting_error);
				publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "Call connect module", null);
				break;
			case CallConnectGlobals.CALL_CONNECT_CHARGING_ERROR:
				severe(String.format("processCallConnectResult method: a charging error returned by call connect module [%d] for %s", result, prettyPrintCallConnectParams(inParams)));
				inputScheduler.raise(inputs.ssp.charging_error);
				publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "Charging module", null);
				break;
			default:
				severe(String.format("processCallConnectResult method: an unexpected result code returned by call connect module [%d] for %s", result, prettyPrintCallConnectParams(inParams)));
				inputScheduler.raise(inputs.ssp.rerouting_error);
				publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "Call connect module", "Unknown error from call connect module");
				break;
		}
	}
	
	private void processPlayAnnouncementInitResult(final int result, final UIInParam annInParams, final UIOutParam annOutParams, final Inputs inputs, final InputScheduler<FSMInput> inputScheduler) {	
		getDefaultSbbUsageParameterSet().incrementUserInteractionInitiated(1L);
		if(isTraceable(TraceLevel.FINEST))
			finest(String.format("processPlayAnnouncementInitResult method: user interaction module result [%d] and output [%s]", result, annOutParams.toString()));

		switch (result) {
			case UIGlobals.UI_WAIT:
				if(isTraceable(TraceLevel.INFO))
					info(String.format("processPlayAnnouncementInitResult method: play ann init succeeded for [AnnId] [%d], user interaction module result code is [%d]", annInParams.getAnncIdList().iterator().next().intValue(), result));
				inputScheduler.raise(inputs.ssp.wait_for_announcement_completion);
				break;
			case UIGlobals.UI_FAILURE:
			case UIGlobals.UI_NO_ANNOUNCEMENT:
	        	severe(String.format("processPlayAnnouncementInitResult method: a general error returned by user interaction module [%d] for [AnnId] [%d]", result, annInParams.getAnncIdList().iterator().next().intValue()));
				inputScheduler.raise(inputs.ssp.ann_error);
				publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "Ann module", null);
				break;
			default:
				severe(String.format("processPlayAnnouncementInitResult method: an unexpected result code returned by user interaction module [%d] for [AnnId] [%d]", result, annInParams.getAnncIdList().iterator().next().intValue()));
				inputScheduler.raise(inputs.ssp.ann_error);
				publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "Ann module", "Unknown error from ann module");
				break;
		}
	}
	

	/*****************************************************************************
	 ********************* FSM actions implementation ****************************
	 *****************************************************************************/
	
	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#sendCUEAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void sendCUEAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		if(isTraceable(TraceLevel.FINEST))
			finest(String.format("sendCUEAction method: preparing to send INAP CUE, dialog's state is [%s]", getDialogStateFromAci(endpoints.ssp.getAci())));

		try {
			final CCDialog dialog = (CCDialog) endpoints.ssp.getAci().getActivity();
			if(!dialog.getDialogState().equals(Dialog.State.IDLE)) {
				dialog.sendContinue();
				publishReportEvent(
						AusEventCodes.EVENT_CUE_SENT,
						StateMachineUtils.getInputsRaised(inputs),
						"State:"+getCurrentState().getStateIdentifier().name());
			}
			inputScheduler.raise(inputs.ssp.CUE_sent);
		} catch (final Exception ex) {
			severe(String.format("sendCUEAction method: an error occured when trying to send INAP CUE"), ex);
			inputScheduler.raise(inputs.ssp.CUE_failed);
			publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "CUE_FAILUE", null);
		}
	}
	
	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#sendRELAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void sendRELAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		if(isTraceable(TraceLevel.FINEST))
			finest(String.format("sendRELAction method: preparing to send INAP REL with cause [%d], dialog's state is [%s]",
							Cause.CauseValue.CLASS1_NORMAL.intValue(),
							getDialogStateFromAci(endpoints.ssp.getAci())));

		try {
			final CCReleaseCallArg relCallArg = new CCReleaseCallArg();
			relCallArg.setInitialCallSegment(new Cause(Cause.CauseValue.CLASS1_NORMAL));
			((CCDialog) endpoints.ssp.getAci().getActivity()).sendReleaseCall(relCallArg);
			publishReportEvent(
					AusEventCodes.EVENT_RC_SENT,
					StateMachineUtils.getInputsRaised(inputs),
					"State:"+getCurrentState().getStateIdentifier().name());
			inputScheduler.raise(inputs.ssp.call_completed);
		} catch (final ProtocolException ex) {
			severe(String.format("sendRELAction method: an error occured when trying to send INAP REL"), ex);
			inputScheduler.raise(inputs.ssp.REL_failed);
			publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "REL_FAILUE", null);
		}
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#findServiceDetailsAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void findServiceDetailsAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		try {
			final ActivityContextInterface ldapAci = LdapUtils.sendLdapQuery(getAusContextCache().getMsisdn(), ldapProvider, ldapAciFactory, AusSbb.this);
			endpoints.cupr.setAci(ldapAci);
			endpoints.cupr.attachToAci();
		} catch (ConnectionException | SearchException ex) {
			inputScheduler.raise(inputs.cupr.ldap_error);
			publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "LDAP", ex.getMessage());
			getAlarmFacility().raiseAlarm(
					String.format("%s.%s", TRACER_NAME, AusAlarms.LDAP_CONNECTION_ERROR.getAlarmType()),
					String.format("resource-adaptor-object-name:%s", LDAP_PROVIDER_JNDI_NAME),
					AusAlarms.LDAP_CONNECTION_ERROR.getAlarmLevel(),
					AusAlarms.LDAP_CONNECTION_ERROR.getFinalMessage("unable to initiate LDAP query"), ex);
		};
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#checkCallProcessingScenarioAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void checkCallProcessingScenarioAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		if(!getAusContextCache().validateInapParamsExtConsistency(AusSbb.this)) {
			publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "INAP", "Missing INAP mandatory params");
			getAlarmFacility().raiseAlarm(
					String.format("%s.%s", AusSbb.TRACER_NAME, AusAlarms.INAP_INCONSISTENT_DATA.getAlarmType()),
					String.format("MSISDN:%s", ausContextCache.getMsisdn()),
					AusAlarms.INAP_INCONSISTENT_DATA.getAlarmLevel(),
					AusAlarms.INAP_INCONSISTENT_DATA.getFinalMessage());
			inputScheduler.raise(inputs.local.internal_error);
			return;
		}
		
		getAusContextCache().computeScenarioType();
		getDefaultSbbUsageParameterSet().incrementServiceStarted(1L);
		publishReportEvent(AusEventCodes.EVENT_SERVICE_CHK_OK);
		if(isTraceable(TraceLevel.INFO))
			info(String.format("checkCallProcessingScenarioAction method: the call is going to be processed according to [%s] scenario", getAusContextCache().getScenario().name()));
		
		if(getAusContextCache().getIsBlocked().booleanValue())
			inputScheduler.raise(inputs.local.call_blocking_enabled);
		else {
			inputScheduler.raise(inputs.local.call_rerouting_enabled);
			
			if(getAusContextCache().getScenario().isFciEnabled())
				inputScheduler.raise(inputs.local.fci_enabled);

			if(getAusContextCache().getScenario().isSciEnabled())
				inputScheduler.raise(inputs.local.sci_enabled);

			if(!getAusContextCache().getScenario().isChargingEnabled())
				inputScheduler.raise(inputs.local.charging_disabled);
		}
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#doReroutingAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void doReroutingAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		dumpAusContext("doReroutingAction");
		final CallConnectInParam callConnectInParam = prepareCallConnectParams(endpoints.ssp.getAci(), false);
		final CallConnectOutParam callConnectOutParam = new CallConnectOutParam();

		try {
			final CallConnectSbbLocalInterface callConnectChildSbb = getCallConnectSbb();
			final int result = callConnectChildSbb.doHunting(callConnectInParam, callConnectOutParam);
			processCallConnectResult(result, callConnectInParam, callConnectOutParam, inputs, inputScheduler);
		} catch(final Exception ex) {
			severe("doReroutingAction method: an unexpected error occured", ex);
			inputScheduler.raise(inputs.local.internal_error);
			publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "Call connect module", ex.getMessage());
		}
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#doReroutingWithChargingAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void doReroutingWithChargingAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		dumpAusContext("doReroutingWithChargingAction");
		final CallConnectInParam callConnectInParam = prepareCallConnectParams(endpoints.ssp.getAci(), true);
		final CallConnectOutParam callConnectOutParam = new CallConnectOutParam();
		
		try {
			final CallConnectSbbLocalInterface callConnectChildSbb = getCallConnectSbb();
			final int result = callConnectChildSbb.doHunting(callConnectInParam, callConnectOutParam);
			processCallConnectResult(result, callConnectInParam, callConnectOutParam, inputs, inputScheduler);
		} catch(final Exception ex) {
			severe("doReroutingWithChargingAction method: an unexpected error occured", ex);
			inputScheduler.raise(inputs.local.internal_error);
			publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "Call connect module", ex.getMessage());
		}
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#doAnnouncementAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void doAnnouncementAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		dumpAusContext("doAnnouncementAction");
		final UIInParam annInParams = prepareAnnouncementParams(endpoints.ssp.getAci());
		final UIOutParam annOutParams = new UIOutParam();
		
		try {
			final IVRInterfaceSbbLocalInterface userInteractionChild = getUserInteractionSbb();
			final int result = userInteractionChild.announcementMessage(annInParams, annOutParams);
			processPlayAnnouncementInitResult(result, annInParams, annOutParams, inputs, inputScheduler);
		} catch(final Exception ex) {
			severe("doAnnouncementAction method: an unexpected error occured", ex);
			inputScheduler.raise(inputs.local.internal_error);
			publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "Ann module", ex.getMessage());
		}
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#processPlayAnnFinalResAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void processPlayAnnFinalResAction(final Inputs inputs,	final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		Object assObj = null;
		UIOutParam annOutParams = null;
		if(inputs.ssp.ann_played.isRaised() && (assObj = inputs.ssp.ann_played.getAssociatedObject()) instanceof UIOutParam) {
			annOutParams = (UIOutParam) assObj;
			if(isTraceable(TraceLevel.INFO))
				info(String.format("processPlayAnnFinalResAction method: play ann succeeded, user interaction module result code is [%d], dialog's state is [%s]", annOutParams.getResultCode(), getDialogStateFromAci(endpoints.ssp.getAci())));
		} else if(inputs.ssp.ann_failed.isRaised() && (assObj = inputs.ssp.ann_failed.getAssociatedObject()) instanceof UIOutParam) {
			annOutParams = (UIOutParam) assObj;
			warn(String.format("processPlayAnnFinalResAction method: play ann failed, obtained results [resCode,errorCode] are [%d,%d], dialog's state is [%s], possible values for UI_FAILURE, UI_FAILURE_DUE_TO_TIMEOUT and UI_IMPROPER_CALLER_RESP are [%d,%d,%d]",
							annOutParams.getResultCode(), annOutParams.getErrorCode(), getDialogStateFromAci(endpoints.ssp.getAci()),
							UIGlobals.UI_FAILURE, UIGlobals.UI_FAILURE_DUE_TO_TIMEOUT, UIGlobals.UI_IMPROPER_CALLER_RESP));
		} else if(inputs.ssp.ann_timeout.isRaised() && (assObj = inputs.ssp.ann_timeout.getAssociatedObject()) instanceof TimerEvent) {
			warn("processPlayAnnFinalResAction method: ann timeout expired, default treatment (CUE) has to be applied!!");
			publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "Timeout", null);
		} else
			warn(String.format("processPlayAnnFinalResAction method: unrecognised input or lack of associated object [%s], should not happen!!", assObj!=null ? assObj.getClass() : "null"));
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#startAnnTimerAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void startAnnTimerAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		final TimerID annTimer = getTimerFacility().setTimer(
				endpoints.ssp.getAci(), null,
				System.currentTimeMillis() + ANN_TIMEOUT,
				new TimerOptions());
		setAnnTimerId(annTimer);
		if(isTraceable(TraceLevel.FINEST))
			finest(String.format("startAnnTimerAction method: timer [%s] for guarding user interaction is started and will expire in [%d] ms", annTimer.toString(), ANN_TIMEOUT));		
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#saveContextInCMPAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void saveContextInCMPAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		dumpAusContext("saveContextInCMPAction");
		setAusContext(getAusContextCache());
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#stopAnnTimerAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void stopAnnTimerAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints,	final Facilities facilities) {
		final TimerID annTimer = getAnnTimerId();
		if(isTraceable(TraceLevel.FINEST))
			finest(String.format("stopAnnTimerAction method: timer [%s] for guarding user interaction is going to be cancelled now", annTimer.toString()));		
		getTimerFacility().cancelTimer(annTimer);
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#closeDialogAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void closeDialogAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		if(isTraceable(TraceLevel.FINEST))
			finest(String.format("closeDialogAction method: dialog will be closed, its current state is [%s]", getDialogStateFromAci(endpoints.ssp.getAci())));

		try {
			//For TCAP Aborts dialog is already closed, the same for ERB(oAbandon)
			final CCDialog dialog = (CCDialog) endpoints.ssp.getAci().getActivity();
			if(!inputs.ssp.user_abort_received.isRaised() && !inputs.ssp.provider_abort_received.isRaised() && !dialog.getDialogState().equals(Dialog.State.IDLE))
				dialog.sendClose(false);
			
			getDefaultSbbUsageParameterSet().sampleExecutionTime(System.currentTimeMillis() - getAusContextCache().getStartTime());
			if(inputs.ssp.call_completed.isRaised())
				getDefaultSbbUsageParameterSet().incrementServiceFinishedOk(1L);
			else
				getDefaultSbbUsageParameterSet().incrementServiceFinishedError(1L);

		} catch(final ProtocolException ex) {
			severe("closeDialogAction method: an unexpected error occured (nothing will be done because service was supposed to terminate anyway)", ex);
			publishReportEvent(AusEventCodes.EVENT_INTERNAL_ERROR, "Dialog closure", ex.getMessage());
			getDefaultSbbUsageParameterSet().incrementServiceFinishedError(1L);
		}
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#unsetDurableInputsAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void unsetDurableInputsAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		inputScheduler.clearAll();
	}

	/**
	 * Implementation of FSM defined action from {@link AusSbbStateMachine}.
	 * @see pl.polkomtel.ngin.aus.services.AusSbbStateMachine.AusSbbActions#clearResourcesAction(pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Inputs, com.opencloud.sce.fsmtool.InputScheduler, pl.polkomtel.ngin.aus.services.AusSbbStateMachine.Endpoints, com.opencloud.sce.fsmtool.Facilities)
	 */
	@Override
	public void clearResourcesAction(final Inputs inputs, final InputScheduler<FSMInput> inputScheduler, final Endpoints endpoints, final Facilities facilities) {
		if(isTraceable(TraceLevel.FINEST))
			finest(String.format("clearResourcesAction method: perform activities cleanup for [%d] activities", getSbbContext().getActivities().length));		
		detachAllActivities();
	}
}
