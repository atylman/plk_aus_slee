package pl.polkomtel.ngin.aus.services.stats;

/**
 * SBB Usage Parameters interface used for statistics generation.
 * 
 * @author adam.tylman
 */
public interface AusUsageParameters {

	/**
     * Increment counter.
     * @param counter increase counter by this value
     */
	void incrementServiceStarted(long counter);

	/**
     * Increment counter.
     * @param counter increase counter by this value
     */
	void incrementServiceFinishedOk(long counter);

	/**
     * Increment counter.
     * @param counter increase counter by this value
     */
	void incrementServiceFinishedError(long counter);

	/**
     * Increment counter.
     * @param counter increase counter by this value
     */
	void incrementUserInteractionInitiated(long counter);

	/**
     * Increment counter.
     * @param counter increase counter by this value
     */
	void incrementConnectAttempts(long counter);

	/**
     * Update sample.
     * @param executionTime update sample by this value in milliseconds
     */
	void sampleExecutionTime(long executionTime);
}
