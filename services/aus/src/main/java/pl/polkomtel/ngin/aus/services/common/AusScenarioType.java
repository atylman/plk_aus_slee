package pl.polkomtel.ngin.aus.services.common;

/**
 * Enumeration that represents all possible scenario types of service execution.
 * 
 * @author adam.tylman
 */
public enum AusScenarioType {
	BLOCKING(0), DIRECT_REROUTING(1), REROUTING_WITH_FCI_ONLY(2), REROUTING_WITH_SCI_ONLY(3), REROUTING_WITH_FCI_SCI(4), UNSPECIFIED(5);
	
	private int numeral;
	
	private AusScenarioType(final int num) {
		this.numeral = num;
	}

	/**
	 * Getter to the numeric value of of the scenario type.
	 * @return numeric value of that enum
	 */
	public int getNumeral() {
		return this.numeral;
	}
	
	/**
	 * Initiates a new enum instance from a numeric value.
	 * @param num numeric value
	 * @return new enum representing such a scenario
	 */
	public static AusScenarioType fromNumeral(final int num) {
		switch (num) {
			case 0: return BLOCKING;
			case 1: return DIRECT_REROUTING;
			case 2: return REROUTING_WITH_FCI_ONLY;
			case 3: return REROUTING_WITH_SCI_ONLY;
			case 4: return REROUTING_WITH_FCI_SCI;
			case 5: 
			default: return UNSPECIFIED;
		}
	}
	
	/**
	 * Verifies if any charging operations (FCI/SCI) are to be sent in such a scenario.
	 * @return true if either FCI or SCI is to be sent, false otherwise
	 */
	public boolean isChargingEnabled() {
		return (this.isFciEnabled() || this.isSciEnabled());
	}
	
	/**
	 * Verifies if FCI operation is to be sent in such a scenario.
	 * @return true if FCI is to be sent, false otherwise
	 */
	public boolean isFciEnabled() {
		return (this.equals(REROUTING_WITH_FCI_ONLY) || this.equals(REROUTING_WITH_FCI_SCI));
	}
	
	/**
	 * Verifies if SCI operation is to be sent in such a scenario.
	 * @return true if SCI is to be sent, false otherwise
	 */
	public boolean isSciEnabled() {
		return (this.equals(REROUTING_WITH_SCI_ONLY) || this.equals(REROUTING_WITH_FCI_SCI));
	}
}