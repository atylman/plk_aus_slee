package pl.polkomtel.ngin.aus.services.alarm;

import javax.slee.facilities.AlarmLevel;

import pl.polkomtel.ngin.aus.services.AusSbb;

import com.nsn.ploc.feniks.slee.nsnbaselib.alarm.ServiceAlarm;

/**
 * Alarms used by {@link AusSbb}
 *
 * @author adam.tylman
 */
public enum AusAlarms implements ServiceAlarm {

	LDAP_CONNECTION_ERROR(AlarmLevel.MAJOR, "LdapConnectionError", "LDAP connection problem: {0}", PARSEABLE),
	LDAP_INCONSISTENT_DATA(AlarmLevel.MAJOR, "LdapInconsistentData", "Manadatory params are missing for LDAP src", NOT_PARSEABLE),
	INAP_INCONSISTENT_DATA(AlarmLevel.MAJOR, "InapInconsistentData", "Manadatory params are missing for INAP src", NOT_PARSEABLE);

	private final AlarmLevel alarmLevel;
	private final String alarmType;
	private final String messagePatttern;
	private boolean parseable = false;

	private AusAlarms(final AlarmLevel alarmLevel, final String alarmType, final String messagePattern, final boolean parseable) {
		this.alarmLevel = alarmLevel;
		this.alarmType = alarmType;
		this.messagePatttern = messagePattern;
		this.parseable = parseable;
	}

	/**
	 * Gets the alarm level
	 * @return the alarm level
	 * @see com.nsn.ploc.feniks.slee.nsnbaselib.alarm.ServiceAlarm#getAlarmLevel()
	 */
	@Override
	public AlarmLevel getAlarmLevel() {
		return this.alarmLevel;
	}

	/**
	 * Gets the alarm type
	 * @return the alarm type
	 * @see com.nsn.ploc.feniks.slee.nsnbaselib.alarm.ServiceAlarm#getAlarmType()
	 */
	@Override
	public String getAlarmType() {
		return this.alarmType;
	}

	/**
	 * Gets the alarm message pattern
	 * @return the alarm message pattern
	 * @see com.nsn.ploc.feniks.slee.nsnbaselib.alarm.ServiceAlarm#getMessagePattern()
	 */
	@Override
	public String getMessagePattern() {
		return this.messagePatttern;
	}

	/**
	 * Indicates if the alarm message is parseable or not
	 * @return <code>true</code> if alarm message is parseable, <code>false</code> otherwise
	 * @see com.nsn.ploc.feniks.slee.nsnbaselib.alarm.ServiceAlarm#isParseable()
	 */
	@Override
	public boolean isParseable() {
		return this.parseable;
	}
	
	/**
	 * Gets the alarm final message
	 * @param params parameters to build final message
	 * @return the alarm message pattern
	 * @see com.nsn.ploc.feniks.slee.nsnbaselib.alarm.ServiceAlarm#getMessagePattern()
	 */
	public String getFinalMessage(final Object... params) {
		if(this.parseable)
			return String.format(messagePatttern, params);
		else
			return messagePatttern;
	}
}
