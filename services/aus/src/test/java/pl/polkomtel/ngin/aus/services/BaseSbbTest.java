package pl.polkomtel.ngin.aus.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.endsWith;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.InitialEventSelector;
import javax.slee.SbbContext;
import javax.slee.facilities.ActivityContextNamingFacility;
import javax.slee.facilities.AlarmFacility;
import javax.slee.facilities.TimerFacility;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;
import javax.slee.nullactivity.NullActivityContextInterfaceFactory;
import javax.slee.nullactivity.NullActivityFactory;
import javax.slee.profile.ProfileFacility;
import javax.slee.serviceactivity.ServiceActivityContextInterfaceFactory;
import javax.slee.serviceactivity.ServiceActivityFactory;

import org.junit.Rule;
import org.junit.rules.TestName;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.stubbing.Stubber;
import org.powermock.reflect.Whitebox;

import pl.polkomtel.ngin.aus.services.common.AusContext;

import com.nsn.gdo.feniks.slee.uan.common.BaseSbb;
import com.nsn.ploc.feniks.slee.mocks.SbbTest;
import com.nsn.ploc.feniks.slee.resources.eventreporter.EventReporterProvider;
import com.opencloud.sce.fsmtool.FSMInput;
import com.opencloud.slee.resources.cgin.Code;
import com.opencloud.slee.resources.cgin.ComponentEvent;
import com.opencloud.slee.resources.cgin.DialogOpenRequestEvent;
import com.opencloud.slee.resources.cgin.SccpAddress;
import com.opencloud.slee.resources.cgin.TcapApplicationContext;
import com.opencloud.slee.resources.cgin.TcapOperation;
import com.opencloud.slee.resources.cgin.callcontrol.CCDialog;
import com.opencloud.slee.resources.cgin.callcontrol.events.CCInitialDPRequestEvent;
import com.opencloud.slee.resources.cgin.etsi_inap_cs1.CS1InitialDPArg;
import com.opencloud.slee.resources.ldap.LDAPActivityContextInterfaceFactory;
import com.opencloud.slee.resources.ldap.LDAPProvider;

public abstract class BaseSbbTest extends SbbTest {
	
	protected AusSbb sbbSpy;
	protected InitialContext initialCtxMock;
	protected AusContext ausCtxSpy;

	@Mock protected CCDialog dialogMock;
	
	@Mock protected AlarmFacility alarmFacilityMock;
	@Mock protected ProfileFacility profileFacilityMock;
	@Mock protected TimerFacility timerFacilityMock;
	@Mock protected ActivityContextNamingFacility acNamingFacilityMock;
	@Mock protected NullActivityContextInterfaceFactory naciFactoryMock;
	@Mock protected NullActivityFactory naFactoryMock;
	@Mock protected ServiceActivityFactory serviceActivityFactoryMock;
	@Mock protected ServiceActivityContextInterfaceFactory serviceActivityContextInterfaceFactoryMock;
	@Mock protected EventReporterProvider reporterMock;
	@Mock protected LDAPProvider ldapProviderMock;
	@Mock protected LDAPActivityContextInterfaceFactory ldapAciFactoryMock;
	
	@Rule
    public TestName testName = new TestName();
	
	protected String dumpCurrentMethodName() {
		System.out.println(String.format("\nTest: %s", testName.getMethodName()));
		return testName.getMethodName();
    }
	
	protected void mockSbbEnvironment(BaseSbb sbb) {
		SbbContext sbbContext = mock(SbbContext.class, Mockito.RETURNS_DEEP_STUBS);
		
		Tracer mockedTracer = getMockedTracer();
		when(sbbContext.getTracer(anyString())).thenReturn(mockedTracer);
		mockTracerAnswear(mockedTracer);
		
		sbb.setSbbContext(sbbContext);
	}
	
	@Override
	protected Tracer getMockedTracer() {
		Tracer mockedTracer = mock(Tracer.class);
		when(mockedTracer.getTracerName()).thenReturn(AusSbb.TRACER_NAME);
		when(mockedTracer.isInfoEnabled()).thenReturn(true);
		when(mockedTracer.isFineEnabled()).thenReturn(true);
		when(mockedTracer.isFinerEnabled()).thenReturn(true);
		when(mockedTracer.isFinestEnabled()).thenReturn(true);
		when(mockedTracer.isTraceable(any(TraceLevel.class))).thenReturn(true);
		return mockedTracer;
	}
	
	private void mockTracerAnswear(Tracer mockedTracer) {
		Stubber doAnswerStub = doAnswer(new Answer<String>() {
			@Override
			public String answer(InvocationOnMock mock) throws Throwable {
				StringBuilder log = new StringBuilder();
				log.append(mock.getMethod().getName());
				log.append(" -> ");

				Object[] arguments = mock.getArguments();
				for (Object argument : arguments) {
					if (argument != null) {
						if(argument instanceof Throwable) {
							Throwable ex = (Throwable) argument;
							StringWriter errorStack = new StringWriter();
							ex.printStackTrace(new PrintWriter(errorStack));
							log.append(errorStack.toString());
							continue;
						}
						
						log.append(argument).append(" ");
					}
				}
				System.out.println(log);
				return null;
			}
		});

		doAnswerStub.when(mockedTracer).trace(any(TraceLevel.class), anyString());
		doAnswerStub.when(mockedTracer).trace(any(TraceLevel.class), anyString(), any(Throwable.class));
		doAnswerStub.when(mockedTracer).finest(anyString());
		doAnswerStub.when(mockedTracer).finest(anyString(), any(Throwable.class));
		doAnswerStub.when(mockedTracer).info(anyString());
		doAnswerStub.when(mockedTracer).info(anyString(), any(Throwable.class));
		doAnswerStub.when(mockedTracer).warning(anyString());
		doAnswerStub.when(mockedTracer).warning(anyString(), any(Throwable.class));
		doAnswerStub.when(mockedTracer).severe(anyString());
		doAnswerStub.when(mockedTracer).severe(anyString(), any(Throwable.class));
	}
	
	protected void mockInitialContext() throws Exception {
		Context mockedCtx = mock(Context.class, Mockito.RETURNS_DEEP_STUBS);
		mockAusJndi(mockedCtx);
		
		initialCtxMock = mock(InitialContext.class, Mockito.RETURNS_DEEP_STUBS);
		mockGeneralJndi(initialCtxMock);
		when(initialCtxMock.lookup("java:comp/env")).thenReturn(mockedCtx);
		whenNew(InitialContext.class).withNoArguments().thenReturn(initialCtxMock);
	}
	
	private void mockGeneralJndi(InitialContext mockedIctx) throws NamingException {
		when(mockedIctx.lookup("java:comp/env/slee/facilities/alarm")).thenReturn(alarmFacilityMock);
		when(mockedIctx.lookup("java:comp/env/slee/facilities/profile")).thenReturn(profileFacilityMock);
		when(mockedIctx.lookup("java:comp/env/slee/facilities/timer")).thenReturn(timerFacilityMock);
		when(mockedIctx.lookup("java:comp/env/slee/facilities/activitycontextnaming")).thenReturn(acNamingFacilityMock);
		when(mockedIctx.lookup("java:comp/env/slee/nullactivity/activitycontextinterfacefactory")).thenReturn(naciFactoryMock);
		when(mockedIctx.lookup("java:comp/env/slee/nullactivity/factory")).thenReturn(naFactoryMock);
		when(mockedIctx.lookup("java:comp/env/slee/serviceactivity/factory")).thenReturn(serviceActivityFactoryMock);
		when(mockedIctx.lookup("java:comp/env/slee/serviceactivity/activitycontextinterfacefactory")).thenReturn(serviceActivityContextInterfaceFactoryMock);
		when(mockedIctx.lookup("java:comp/env/slee/resources/er/provider")).thenReturn(reporterMock);
	}
	
	private void mockAusJndi(Context mockedCtx) throws NamingException {
		when(mockedCtx.lookup("slee/resources/er/provider")).thenReturn(reporterMock);
		when(mockedCtx.lookup("slee/resources/ldap/provider")).thenReturn(ldapProviderMock);
		when(mockedCtx.lookup("slee/resources/ldap/acifactory")).thenReturn(ldapAciFactoryMock);
		when(mockedCtx.lookup(endsWith("Error"))).thenReturn(anyString());
	}

	protected InitialEventSelector mockInitialEventSelector(TcapApplicationContext acn) {
		DialogOpenRequestEvent event = mockDialogOpenEvent(acn, mockInitialDpEvent());
		InitialEventSelector ies = mock(InitialEventSelector.class, Mockito.RETURNS_DEEP_STUBS);
		when(ies.getEvent()).thenReturn(event);
		return ies;
	}
	
	protected DialogOpenRequestEvent mockDialogOpenEvent(TcapApplicationContext acn, ComponentEvent comp) {
		return new DialogOpenRequestEvent(
				dialogMock, new SccpAddress(SccpAddress.Type.C7), new SccpAddress(SccpAddress.Type.C7),
				true, 14485, true, 14602, acn, null, new ComponentEvent[] {comp});
	}
	
	protected CCInitialDPRequestEvent mockInitialDpEvent() {
		return new CCInitialDPRequestEvent(dialogMock, createDummyTcapOper(), new CS1InitialDPArg(), 0, false, 0);
	}
	
	protected TcapOperation createDummyTcapOper() {
		return new TcapOperation(null, null, "", "", "", null,
				new Code().setLocal(0), TcapOperation.OperationClass.CLASS1,
				null, false, null, false, null, null, null, null, null, null);
	}
	
	protected void assertFsmInputRaised(String inputName) {
		ArgumentCaptor<FSMInput> inputCaptor = ArgumentCaptor.forClass(FSMInput .class);
		verify(sbbSpy).raiseInputAndExecuteFsm(inputCaptor.capture(), anyObject());
		assertEquals(inputName, inputCaptor.getValue().toString());
	}
	
	protected void createValidAusCtx() {
		Whitebox.setInternalState(ausCtxSpy, "msisdn", "48600123456");
		Whitebox.setInternalState(ausCtxSpy, "calledNumber", "48600999999");
		Whitebox.setInternalState(ausCtxSpy, "calledNumberTON", 9);
		Whitebox.setInternalState(ausCtxSpy, "callingNumberFormatted", "48600333333");
		Whitebox.setInternalState(ausCtxSpy, "calledNumberTONFormatted", 8);
		Whitebox.setInternalState(ausCtxSpy, "calledNumberNumberingPlan", 6);
		Whitebox.setInternalState(ausCtxSpy, "calledNumberRoutingToInternalNetwork", 5);
		Whitebox.setInternalState(ausCtxSpy, "callingPartyCategory", (byte) 3);
		Whitebox.setInternalState(ausCtxSpy, "calledNumberFormatted", "48600888888");
		Whitebox.setInternalState(ausCtxSpy, "calledNumberTONFormatted", 2);
		Whitebox.setInternalState(ausCtxSpy, "locationInfo", new byte[] { (byte) 0xAA, (byte) 0xBB, (byte) 0xCC });
	}
	
	protected void createFullAusCtx() {
		createValidAusCtx();
		Whitebox.setInternalState(ausCtxSpy, "callingNumber", "48600111111");
		Whitebox.setInternalState(ausCtxSpy, "callingNumberTON", 1);
		Whitebox.setInternalState(ausCtxSpy, "redirectingNumberFormatted", "48600222222");
		Whitebox.setInternalState(ausCtxSpy, "redirectingNumberTONFormatted", 2);
		Whitebox.setInternalState(ausCtxSpy, "serviceKey", 1000);
		Whitebox.setInternalState(ausCtxSpy, "imsi", "123456");
		
		Whitebox.setInternalState(ausCtxSpy, "isBlocked", Boolean.FALSE);
		Whitebox.setInternalState(ausCtxSpy, "prefixNumber", "123");
		Whitebox.setInternalState(ausCtxSpy, "announcementId", Integer.valueOf(1));
		Whitebox.setInternalState(ausCtxSpy, "chargingSciId", Integer.valueOf(2));
		Whitebox.setInternalState(ausCtxSpy, "chargingFciId", Integer.valueOf(3));
		Whitebox.setInternalState(ausCtxSpy, "serviceIdentifier", "123456789");
	}
}