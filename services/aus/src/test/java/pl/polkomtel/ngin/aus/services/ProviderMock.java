package pl.polkomtel.ngin.aus.services;

import com.opencloud.slee.resources.cgin.CGINProvider;
import com.opencloud.slee.resources.sis.script.activity.CompositionActivityExtensionsProvider;
import com.opencloud.slee.resources.sis.script.activity.CompositionActivityProvider;

public interface ProviderMock extends CGINProvider, CompositionActivityExtensionsProvider {
}
