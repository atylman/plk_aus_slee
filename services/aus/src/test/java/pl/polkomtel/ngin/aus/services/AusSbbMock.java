package pl.polkomtel.ngin.aus.services;

import static org.powermock.api.mockito.PowerMockito.mock;

import javax.slee.ActivityContextInterface;
import javax.slee.ChildRelation;
import javax.slee.EventContext;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.TimerID;
import javax.slee.facilities.TraceLevel;

import pl.polkomtel.ngin.aus.services.common.AusContext;
import pl.polkomtel.ngin.aus.services.stats.AusUsageParameters;

import com.nsn.ploc.feniks.slee.resources.eventreporter.SessionTraceContext;
import com.opencloud.sce.fsmtool.FSMInput;
import com.opencloud.slee.resources.cgin.callcontrol.events.CCInitialDPRequestEvent;

public class AusSbbMock extends AusSbb {
	
	public AusSbbMock() {
		sessionId = new SessionTraceContext("101-1422628754-49260", AusSbb.TRACER_NAME, false);
    }
	
	@Override
	protected void raiseInputAndExecuteFsm(final FSMInput input, final Object event) {
		System.out.println("raiseInputAndExecuteFsm: input raised --> "+input);
    }
	
	@Override
	public AusUsageParameters getDefaultSbbUsageParameterSet() {
		return mock(AusUsageParameters.class);
	}
	
	@Override
	protected boolean prepareTraceContext(final CCInitialDPRequestEvent idp) {
		return true;
	}
	
	@Override
	protected boolean prepareInapContext(final CCInitialDPRequestEvent idp) {
		return true;
	}

	@Override
	public AusContext getAusContext() {
		return null;
	}

	@Override
	public void setAusContext(AusContext ausCtx) {
	}

	@Override
	public TimerID getAnnTimerId() {
		return null;
	}

	@Override
	public void setAnnTimerId(TimerID timerId) {
	}

	@Override
	public SessionTraceContext getSessionIdImpl() {
		return sessionId;
	}

	@Override
	public void setSessionIdImpl(SessionTraceContext sessionID) {
	}

	@Override
	public TraceLevel getSubscriberTraceLevel() {
		return TraceLevel.OFF;
	}

	@Override
	public void setSubscriberTraceLevel(TraceLevel traceLevel) {
	}

	@Override
	public ChildRelation getCallConnectSbbRelation() {
		return null;
	}

	@Override
	public ChildRelation getUserInteractionSbbRelation() {
		return null;
	}

	@Override
	public void setInputObject_ssp_call_completed_cmp(Object ssp_call_completed) {
	}

	@Override
	public Object getInputObject_ssp_call_completed_cmp() {
		return null;
	}

	@Override
	public void setInputObject_ssp_user_abort_received_cmp(Object ssp_user_abort_received) {
	}

	@Override
	public Object getInputObject_ssp_user_abort_received_cmp() {
		return null;
	}

	@Override
	public void setInputObject_ssp_provider_abort_received_cmp(Object ssp_provider_abort_received) {
	}

	@Override
	public Object getInputObject_ssp_provider_abort_received_cmp() {
		return null;
	}

	@Override
	public void setAci_local(ActivityContextInterface local) {
	}

	@Override
	public ActivityContextInterface getAci_local() {
		return null;
	}

	@Override
	public void setAci_ssp(ActivityContextInterface ssp) {
	}

	@Override
	public ActivityContextInterface getAci_ssp() {
		return null;
	}

	@Override
	public void setAci_cupr(ActivityContextInterface cupr) {
	}

	@Override
	public ActivityContextInterface getAci_cupr() {
		return null;
	}

	@Override
	public void setEventContext_local(EventContext local) {
	}

	@Override
	public EventContext getEventContext_local() {
		return null;
	}

	@Override
	public void setEventContext_ssp(EventContext ssp) {
	}

	@Override
	public EventContext getEventContext_ssp() {
		return null;
	}

	@Override
	public void setEventContext_cupr(EventContext cupr) {	
	}

	@Override
	public EventContext getEventContext_cupr() {
		return null;
	}

	@Override
	public void setSbbLocalObject_local(SbbLocalObject local) {
	}

	@Override
	public SbbLocalObject getSbbLocalObject_local() {
		return null;
	}

	@Override
	public void setSbbLocalObject_ssp(SbbLocalObject ssp) {
	}

	@Override
	public SbbLocalObject getSbbLocalObject_ssp() {
		return null;
	}

	@Override
	public void setSbbLocalObject_cupr(SbbLocalObject cupr) {
	}

	@Override
	public SbbLocalObject getSbbLocalObject_cupr() {
		return null;
	}

	@Override
	public void setSessionTracerPrefixCMP(String SessionTracerPrefix) {
	}

	@Override
	public String getSessionTracerPrefixCMP() {
		return null;
	}

	@Override
	public void setStandardTracerPrefixCMP(String stdTracerPrefix) {
	}

	@Override
	public String getStandardTracerPrefixCMP() {
		return null;
	}

	@Override
	public void setSessionTracerEnabledCMP(boolean SessionTracerEnabled) {
	}

	@Override
	public boolean getSessionTracerEnabledCMP() {
		return false;
	}

	@Override
	public void setStoredState(int storedState) {
	}

	@Override
	public int getStoredState() {
		return 0;
	}

	@Override
	public void setEncodedInputRegister(byte[] encodedInputRegister) {
	}

	@Override
	public byte[] getEncodedInputRegister() {
		return null;
	}
}
