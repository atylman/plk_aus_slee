package pl.polkomtel.ngin.aus.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import javax.slee.ActivityContextInterface;
import javax.slee.CreateException;
import javax.slee.InitialEventSelector;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import pl.polkomtel.ngin.aus.services.common.AusContext;
import pl.polkomtel.ngin.aus.services.event.AusEventCodes;

import com.opencloud.slee.resources.cgin.CGINProvider;
import com.opencloud.slee.resources.cgin.DialogOpenRequestEvent;
import com.opencloud.slee.resources.cgin.callcontrol.CCAssistRequestInstructionsArg;
import com.opencloud.slee.resources.cgin.callcontrol.events.CCAssistRequestInstructionsRequestEvent;
import com.opencloud.slee.resources.cgin.callcontrol.events.CCInitialDPRequestEvent;
import com.opencloud.slee.resources.cgin.etsi_inap_cs1.metadata.CS1ApplicationContexts;
import com.opencloud.slee.resources.cgin.nokia_inap_cs1.metadata.NCS1ApplicationContexts;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames={"pl.polkomtel.ngin.aus.services.*", "com.nsn.gdo.feniks.slee.uan.common.*"})
public class AusSbbTest extends BaseSbbTest {

	@Before
    public final void setUp() throws CreateException, Exception {
		dumpCurrentMethodName();
		MockitoAnnotations.initMocks(this);
		sbbSpy = spy(new AusSbbMock());
		ausCtxSpy = spy(new AusContext());
		mockInitialContext();
		mockSbbEnvironment(sbbSpy);
    }
	
	@Test
    public final void verifyInitialEventAccepted() throws Exception {
		InitialEventSelector ies = mockInitialEventSelector(NCS1ApplicationContexts.core_INAP_CS1_SSP_to_SCP_AC);
		sbbSpy.initialEventSelect(ies);
		verify(ies).setInitialEvent(eq(true));
	}
	
	@Test
    public final void verifyInitialEventRejectedDueToWrongAcn() throws Exception {
		InitialEventSelector ies = mockInitialEventSelector(CS1ApplicationContexts.core_INAP_CS1_SSP_to_SCP_AC);
		sbbSpy.initialEventSelect(ies);
		verify(ies).setInitialEvent(eq(false));
	}
	
	@Test
    public final void verifyInitialEventProcessingErrorDueToWrongOperation() throws Exception {
		DialogOpenRequestEvent event = mockDialogOpenEvent(
				CS1ApplicationContexts.core_INAP_CS1_SSP_to_SCP_AC,
				new CCAssistRequestInstructionsRequestEvent(dialogMock, createDummyTcapOper(), new CCAssistRequestInstructionsArg(), 0, false, 0));
		sbbSpy.onOpenRequest(event, mock(ActivityContextInterface.class));
		
		verify(sbbSpy).publishReportEvent(eq(AusEventCodes.EVENT_INTERNAL_ERROR), anyString(), anyString());
		assertFsmInputRaised("local_context_creation_failed");
	}
	
	@Test
    public final void verifyInitialEventProcessingErrorDueToWrongActivityProvider() throws Exception {
		when(dialogMock.getProvider()).thenReturn(mock(CGINProvider.class));
		DialogOpenRequestEvent event = mockDialogOpenEvent(
				CS1ApplicationContexts.core_INAP_CS1_SSP_to_SCP_AC,
				mockInitialDpEvent());
		sbbSpy.onOpenRequest(event, mock(ActivityContextInterface.class));
		
		verify(sbbSpy).publishReportEvent(eq(AusEventCodes.EVENT_INTERNAL_ERROR), anyString(), anyString());
		assertFsmInputRaised("local_context_creation_failed");
	}
	
	@Test
    public final void verifyInitialEventProcessingErrorDueToMissingParams() throws Exception {
		when(dialogMock.getProvider()).thenReturn(mock(ProviderMock.class));
		DialogOpenRequestEvent event = mockDialogOpenEvent(
				CS1ApplicationContexts.core_INAP_CS1_SSP_to_SCP_AC,
				mockInitialDpEvent());
		//remark: if when(...).thenReturn(...) syntax is used, a real method will be called 
		doReturn(false).when(sbbSpy).prepareInapContext(any(CCInitialDPRequestEvent.class));
		sbbSpy.onOpenRequest(event, mock(ActivityContextInterface.class));
		verify(sbbSpy).publishReportEvent(eq(AusEventCodes.EVENT_INTERNAL_ERROR), anyString(), anyString());
		assertFsmInputRaised("local_context_creation_failed");
	}
	
	@Test
    public final void verifyInitialEventProcessingSuccess() throws Exception {
		when(dialogMock.getProvider()).thenReturn(mock(ProviderMock.class));
		DialogOpenRequestEvent event = mockDialogOpenEvent(
				CS1ApplicationContexts.core_INAP_CS1_SSP_to_SCP_AC,
				mockInitialDpEvent());
		sbbSpy.onOpenRequest(event, mock(ActivityContextInterface.class));
		verify(sbbSpy).publishReportEvent(eq(AusEventCodes.EVENT_IDP_RECEIVED));
		assertFsmInputRaised("local_context_creation_success");
	}

	@Test
	public final void verifyLdapParamsConsistencyValidation() {
		//isBlocked == null
		assertFalse(ausCtxSpy.validateLdapParamsConsistency());
		
		//isBlocked.booleanValue() && announcementId == null
		ausCtxSpy.setIsBlocked(Boolean.TRUE);
		assertFalse(ausCtxSpy.validateLdapParamsConsistency());
		
		//isBlocked.booleanValue() && announcementId != null
		ausCtxSpy.setAnnouncementId(Integer.valueOf(1));
		assertTrue(ausCtxSpy.validateLdapParamsConsistency());
		
		//!isBlocked.booleanValue() && StringUtils.isBlank(prefixNumber)
		ausCtxSpy.setIsBlocked(Boolean.FALSE);
		assertFalse(ausCtxSpy.validateLdapParamsConsistency());
		
		//!isBlocked.booleanValue() && StringUtils.isNotBlank(prefixNumber)
		ausCtxSpy.setPrefixNumber("1234");
		assertTrue(ausCtxSpy.validateLdapParamsConsistency());
		
		//!isBlocked.booleanValue() && StringUtils.isNotBlank(prefixNumber) && chargingFciId != null && StringUtils.isBlank(serviceIdentifier)
		ausCtxSpy.setChargingFciId(Integer.valueOf(11));
		assertFalse(ausCtxSpy.validateLdapParamsConsistency());
		
		//!isBlocked.booleanValue() && StringUtils.isNotBlank(prefixNumber) && chargingFciId != null && StringUtils.isNotBlank(serviceIdentifier)
		ausCtxSpy.setServiceIdentifier("123456");
		assertTrue(ausCtxSpy.validateLdapParamsConsistency());	
	}

	@Test
	public final void verifyInapParamsConsistencyValidation() {
		assertFalse(ausCtxSpy.validateInapParamsBasicConsistency(sbbSpy));
		assertFalse(ausCtxSpy.validateInapParamsExtConsistency(sbbSpy));
		createValidAusCtx();
		assertTrue(ausCtxSpy.validateInapParamsBasicConsistency(sbbSpy));
		assertTrue(ausCtxSpy.validateInapParamsExtConsistency(sbbSpy));
	}
	
	@Test
	public final void verifyAusContextSerialization() throws IOException {
		createFullAusCtx();

		ByteArrayOutputStream ctxSerializedStream = new ByteArrayOutputStream();
		ausCtxSpy.toStream(new DataOutputStream(ctxSerializedStream));

		ByteArrayInputStream ctxDeserializedStream = new ByteArrayInputStream(ctxSerializedStream.toByteArray());
		AusContext ctxDeserialized = new AusContext(new DataInputStream(ctxDeserializedStream));

		System.out.println("AusContext before/after serialization:\n" + ausCtxSpy.toString() + "\n" + ctxDeserialized.toString());
		assertEquals(ausCtxSpy.toString(), ctxDeserialized.toString());
	}
}